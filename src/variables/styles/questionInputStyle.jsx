const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    fontFamily: "andalus"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: "100%",
  },
  fab: {
   position: 'absolute',
   left: theme.spacing.unit * 2
 },
  button: {
   margin: theme.spacing.unit,
 },
 status:{
   marginTop:"1mm",
   marginRight:"-5.5mm"
 },
  menu: {
    width: "100%",
  },
});
export default styles;
