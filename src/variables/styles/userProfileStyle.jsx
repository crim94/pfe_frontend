const profileStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 10,
  },
  heading: {
    fontSize: theme.typography.pxToRem(18),
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: "andalus"
  },  textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: "100%",
      marginTop:  theme.spacing.unit * 3,
    },
  underline: {
    "&:before": {
      backgroundColor: "red",
      height: "1px !important"
    }
  }
});
export default profileStyles;
