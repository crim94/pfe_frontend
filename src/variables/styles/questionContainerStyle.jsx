const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "0px",
    right: "0px",
    zIndex: "10"
  },
  secondButton:{
    margin: theme.spacing.unit,
    position: "fixed",
    bottom: "0px",
    right: "0px",
    marginRight: "70px"
  }
});

export default styles;
