// ##############################
// // // Footer styles
// #############################

import { defaultFont, primaryColor } from "variables/styles";

const footerStyle = {
  footerlogin: {
    bottom: "0",
    width:"100%",
    ...defaultFont
  },
  red:{
    backgroundColor:"#ed1b24",
    border:"1px solid snow"
  },
  center:{
    textAlign:"center"
  },
  right:{
    textAlign:"right"
  },
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent"
  },
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  inlineBlock: {
    display: "inline-block",
    paddingTop: "0px",
    width: 'auto'
  },img:{
    width:"100px"
  },snow:{
    backgroundColor:"snow",
    border:"1px solid #ed1b24"
  }
};
export default footerStyle;
