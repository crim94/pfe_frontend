const loginStyle = theme => ({
  root: theme.mixins.gutters({

    paddingTop: 16,
    paddingBottom: 16,
    marginLeft:'25%',
    marginTop: theme.spacing.unit * 12,
    width: '40%'
  }),  button: {
    margin: theme.spacing.unit,
  },
  bigAvatar: {
   width: 100,
   height: 100,
 }  ,row: {
     display: 'flex',
     justifyContent: 'center',
   },
   avatar: {
     margin: 10,
   },hr:{
     marginTop: 10
   },
   main:{
     marginRight: '10%',
     marginLeft: '10%'
   }, container: {

  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%',
  },
  leftIcon: {
   marginRight: theme.spacing.unit,
 },
 rightIcon: {
   marginLeft: theme.spacing.unit,
 },
 iconSmall: {
   fontSize: 20,
 },
 conteneur:{

}

});
export default loginStyle;
