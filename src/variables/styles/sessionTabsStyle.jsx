const tabsStyles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  form:{
    marginTop:"3mm"
  },
  textField: {
    marginLeft: "4%",
    marginRight:"4%",
    width: "92%",
  },textFieldNb:{
    width: "45%",
    marginRight: "2%"
  },
  textFieldCag:{
    marginLeft: "5%",
    marginRight: "2%",

    width: "43%"
  },

  dashed:{
    border:"1px dashed black"
  }, margin: {
    margin: theme.spacing.unit,
  },
  menu: {
    width: 200,
  }, button: {
    margin: theme.spacing.unit,
    marginLeft:"42%"
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});
export default tabsStyles;
