import App from "containers/App/App.jsx";
import Login from "../components/login/login.jsx";


const indexRoutes = [{ path: "/dashboard", component: App},{
  path:"/login",component:Login
},{
  path:"*",component:Login
}];

export default indexRoutes;
