import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";



import StudioContainer from "views/studio/StudioContainer.jsx";
import StudioAn from "views/studio_animateur/StudioAn.jsx";
//import MediaThequeContainer from "views/Mediatheque/MediaTheque.jsx";
//import SessionPage from "views/Session/SessionPage.jsx";
import SessionTimeLine from "views/Session/SessionTimeLine.jsx";
import TableSession from "views/tables/TableSession.jsx";
import {
  Dashboard,
  Person,
  
EventNote,Theaters

} from "material-ui-icons";

var role;
if(localStorage.getItem("user"))
  {role = (JSON.parse(localStorage.user)).role;
    console.log(role);
    }
var appRoutes = [];
if(role === "animateur"){
  appRoutes = [
    {
      path: "/dashboard/accueil",
      sidebarName: "Accueil",
      navbarName: "Accueil",
      icon: Dashboard,
      component: DashboardPage
    },
    {
      path: "/dashboard/live",
      sidebarName: "En directe",
      navbarName: "En directe",
      icon: Theaters,
      component: StudioAn
    },
    {
      path: "/dashboard/user",
      sidebarName: "Configuration",
      navbarName: "Configuration",
      icon: Person,
      component: UserProfile
    },
    { redirect: true, path: "/dashboard", to: "/dashboard/accueil", navbarName: "Redirect" }
  ];
}else if (role === "editeur"){
  appRoutes = [
    {
      path: "/dashboard/accueil",
      sidebarName: "Accueil",
      navbarName: "Accueil",
      icon: Dashboard,
      component: DashboardPage
    },
    {
      path: "/dashboard/studio",
      sidebarName: "Studio",
      navbarName: "Studio",
      icon: Theaters,
      component: StudioContainer
    },
    {
      path: "/dashboard/user",
      sidebarName: "Configuration",
      navbarName: "Configuration",
      icon: Person,
      component: UserProfile
    },
    { redirect: true, path: "/dashboard", to: "/dashboard/accueil", navbarName: "Redirect" }
  ];
}
else { //super-admin
  appRoutes = [
    {
      path: "/dashboard/accueil",
      sidebarName: "Accueil",
      navbarName: "Accueil",
      icon: Dashboard,
      component: DashboardPage
    },{
      path: "/dashboard/sessions",
      sidebarName: "Sessions",
      navbarName: "Gestion des sessions",
      icon: EventNote,
      component: TableSession
    },
     {
      path: "/dashboard/user",
      sidebarName: "Configuration",
      navbarName: "Configuration",
      icon: Person,
      component: UserProfile
    },

    {
      path: "/dashboard/timeline",
      sidebarName: "Timeline",
      navbarName: "Nouvelle Session",
      icon: Person,
      component: SessionTimeLine,
      notSidebar:true
    },
   { redirect: true, path: "/dashboard", to: "/dashboard/accueil", navbarName: "Redirect" }
  ];

}
export default appRoutes;
