const globale={
  backend_server:{
    port:-1,
    //uri:"https://bo-vittoria.itforme.tn"
    uri:"http://54.38.182.175:6060"
  },
  streaming_server:{
  uri:"http://54.38.182.175:8443"
  },
  API:{
    LOGIN_API: "/admin",
    CATEGORIE_API:"/categorie",
    QUESTION_API:"/question",
    SESSION_API:"/session",
    STATISTIC:"/statistic"
  }
}
export default globale;
