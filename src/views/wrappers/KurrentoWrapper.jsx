import React, { Component } from 'react';
import PropTypes from 'prop-types';
import KurrentoClient from "../../assets/kurento_asset_js/kurento-client/js/kurento-client.js";
import kurentoUtils from "../../assets/kurento_asset_js/kurento-utils/js/kurento-utils.js";
import { withStyles } from 'material-ui/styles';
import {Button} from "material-ui";


const styles ={
  
  button: {
    margin: "2px",
  },
  input: {
    display: 'none',
  },
}
class KurrentoWrapper extends Component {
constructor(props){
    super(props);
    this.classes = this.props.classes;
}
  shouldComponentUpdate() {
    return false;
  }
  componentDidMount() {
    //initialize plugin
    this.localStream = this.refs.videoInput;

  
  }
  componentWillReceiveProps(nextProps){

  }


setIceCandidateCallbacks(webRtcPeer, webRtcEp, onerror)
{
  webRtcPeer.on('icecandidate', function(candidate) {
    console.log("Local candidate:",candidate);

    candidate = kurentoClient.getComplexType('IceCandidate')(candidate);

    webRtcEp.addIceCandidate(candidate, onerror)
  });
  webRtcEp.on('OnIceCandidate', function(event) {
    var candidate = event.candidate;

    console.log("Remote candidate:",candidate);

    webRtcPeer.addIceCandidate(candidate, onerror);
  });
}

handleClick(){
  var options = {
    localVideo: this.refs.videoInput  //  remoteVideo: this.refs.videoOutput
  };

 webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options, function(error) {
     if(error) return onError(error)
     this.generateOffer(onOffer)
  });
}
handleStop(){
  if (webRtcPeer) {
    webRtcPeer.dispose();
    webRtcPeer = null;
  }

  if(pipeline){
    pipeline.release();
    pipeline = null;
  }
}
  render() {
    return (
  
       <div >
          <video ref="videoInput" autoPlay width="480px" height="360px" ></video>
          <Button variant="contained" className={this.classes.button} onClick={this.handleClick.bind(this)} >start

            </Button>
        
        </div>
      



    );
  }
}
//#region  "Kurento methods"

function onError(error) {
  if(error)
  {
    console.error(error);
    stop();
  }
}
function getopts(args, opts)
{
  var result = opts.default || {};
  args.replace(
      new RegExp("([^?=&]+)(=([^&]*))?", "g"),
      function($0, $1, $2, $3) { result[$1] = decodeURI($3); });

  return result;
};
function setIceCandidateCallbacks(webRtcPeer, webRtcEp, onerror)
{
  webRtcPeer.on('icecandidate', function(candidate) {
    console.log("Local candidate:",candidate);

    candidate = kurentoClient.getComplexType('IceCandidate')(candidate);

    webRtcEp.addIceCandidate(candidate, onerror)
  });

  webRtcEp.on('OnIceCandidate', function(event) {
    var candidate = event.candidate;

    console.log("Remote candidate:",candidate);

    webRtcPeer.addIceCandidate(candidate, onerror);
  });
}

var args = getopts(location.search,
{
  default:
  {
    ws_uri: 'ws://localhost:8888/kurento',
    ice_servers: undefined
  }
});
var pipeline;
var webRtcPeer;
function onOffer(error, sdpOffer)
{
  if(error) return onError(error)

  kurentoClient(args.ws_uri, function(error, client)
  {
    if(error) return onError(error);

    client.create("MediaPipeline", function(error, _pipeline)
    {
      if(error) return onError(error);

      pipeline = _pipeline;

      pipeline.create("WebRtcEndpoint", function(error, webRtc){
        if(error) return onError(error);

        setIceCandidateCallbacks(webRtcPeer, webRtc, onError)

        webRtc.processOffer(sdpOffer, function(error, sdpAnswer){
          if(error) return onError(error);

          webRtcPeer.processAnswer(sdpAnswer, onError);
        });
        webRtc.gatherCandidates(onError);

        webRtc.connect(webRtc, function(error){
          if(error) return onError(error);

          console.log("Loopback established");
        });
      });
    });
  });
}


//#endregion
KurrentoWrapper.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(KurrentoWrapper);
