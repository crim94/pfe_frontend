import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit * 3,
    backgroundColor: theme.palette.background.paper,
  },
});

class QuizEditor extends React.Component {


  render() {
    const { classes } = this.props;


    return (
      <div className={classes.root}>
      </div>
    );
  }
}

QuizEditor.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(QuizEditor);
