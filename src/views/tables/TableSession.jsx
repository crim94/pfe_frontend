import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles
  } from "material-ui";

import IconButton from 'material-ui/IconButton';
import Button from "material-ui/Button";
import EditIcon from "material-ui-icons/Edit";
import DeleteIcon from "material-ui-icons/Delete";
import ReactTable from "react-table";
import 'react-table/react-table.css';
import AddIcon from "material-ui-icons/Add";
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import PlusOneIcon from "material-ui-icons/PlusOne";
import SchoolIcon from "material-ui-icons/School";
import PortraitIcon from "material-ui-icons/Portrait";
import HeadestMicIcon from "material-ui-icons/HeadsetMic";
import MoodBadIcon from "material-ui-icons/MoodBad";
import Dialog, { DialogActions, DialogContent, DialogTitle,DialogContentText } from 'material-ui/Dialog';
import axios from 'axios';
import globale from "../../config.js";
let container = null;
const styles =theme => ({
    button: {
        backgroundColor:"lightblue",
        marginLeft:"2px",
        marginBottom:"5px"
       
      }, chip: {
       
      },
      extendedIcon: {
        marginRight: theme.spacing.unit,
        color:"white",
    marginLeft:"5px" },
    textCentred : {
        textAlign:'center'
    },
    infoItem:{
        marginBottom:'5px',
        
      
    },
    fabButton:{
      margin: theme.spacing.unit,
      position: "fixed",
      bottom: "0px",
      right: "2px",
      zIndex: "10"
    }

 });
  const columns = 
      [{
    Header: 'Session',
    accessor: 'num' // String-based value accessors!
  }, {
    Header: 'date',
    accessor: 'date',
    Cell: props => <span>{props.value}</span> // Custom cell components!
  },{
    Header: 'cagnotte',
    accessor: 'cagnotte',
    Cell: props => <span className='number'>{props.value}</span> // Custom cell components!
  }, 
      {
        Header: 'etat',
        accessor: 'etat',
        filterable: false,
        Cell: row => (
          <span>
            <span style={{
              color: row.value === 0 ? '#ff2e00'
                : row.value === 1 ? '#ffbf00'
                : '#57d500',
              transition: 'all .3s ease'
            }}>
              &#x25cf;
            </span> {
              row.value === 0 ? 'terminée'
              : row.value === 1 ? 'planifiée'
              : 'autre'
            }
          </span>
        )
      },{
        columns: [
          {
            expander: true,
            Header: () => <strong>Détails</strong>,
            width: 65,
            Expander: ({ isExpanded, ...rest }) =>
              <div>
                {isExpanded
                  ? <span>&#x2299;</span>
                  : <span>&#x2295;</span>}
              </div>,
            style: {
              cursor: "pointer",
              fontSize: 25,
              padding: "0",
              textAlign: "center",
              userSelect: "none"
            }
          }
      ] 
          }];
          
class TableSession extends React.Component {

    constructor(props){
        super(props);
        container = this;
        this.classes = props.classes;
        this.state = {
            expanded : 0,
            page:0,
            data:[{
              num: 1,
              cagnotte: 2000,
              date:"23/09/1994",
              etat:0
            },{
              num: 2,
              cagnotte: 8000,
              date:"23/09/1994",
              etat:1
            }],
            asupprimer:{},
            open:false,
            infos:[{nb_loser:5,nb_winner:10,animateur:"selmi",editeur:"crim"},{nb_loser:15,nb_winner:10,animateur:"tox",editeur:"omar"}]
        }
    }
    handleSupprimer(){
      var mytoken = localStorage.getItem("admin-token");
      let api=globale.backend_server.uri+  globale.API.SESSION_API+"/delete";
      axios.delete(api,{ headers: { Authorization: "Bearer "+ mytoken},params: {_id:container.state.asupprimer._id} }).then(function(response){
        let data = container.state.data.slice(0);
        data.splice((container.state.asupprimer.num)-1,1);
        container.setState({data:data,open:false,asupprimer:{}});
      }).catch(function(error){
        console.log(error);
      });
    }
    _renderDialog(){
      return (
        <Dialog
          open={this.state.open}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {"Confirmation Suppression"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
            Voulez vous supprimer cette session ?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Annuler
            </Button>
            <Button onClick={this.handleSupprimer.bind(this)} color="primary">
              Confirmer
            </Button>
          </DialogActions>
        </Dialog>
      )
    }
    handleClickOpen(itemData){
      this.setState({ open: true,asupprimer:itemData });
    };
  
    handleClose = () => {
      this.setState({ open: false });
    };
  
    componentWillMount(){
      var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.SESSION_API+"/listSession";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      if(response.status===200){
        //afficher notification & vider question
        var data =response.data;
        for (var i=0;i<data.length;i++){
          data[i].num = (i+1);
          data[i].date =  data[i].date.replace(":00.000Z","");
           
        }
      container.setState({data:data});
      }
    }).catch(function(error){
      console.log(error);
    });
    // info data
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/winnerAndLoser";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      if(response.status===200){
        //afficher notification & vider question
        var info =response.data;
       
      container.setState({info:info});
      }
    }).catch(function(error){
      console.log(error);
    });
      }
    handleExpand(newExpanded, index, event){
   
      for (var property in newExpanded) {
        if(property !== index) newExpanded[property] = false;
      }
      if(!newExpanded[index]===false)this.setState({expanded:index});
    }
    renderSubComponent(){
      var indexRow = container.state.expanded;
      var pageIndex =container.state.page;
      var index = (pageIndex*5) + parseInt(indexRow,10);
      var item = container.state.info[index];
      var itemData = container.state.data[index];
      console.log(itemData);
      if(itemData.etat === 1)
        return (
            <div style={{marginLeft:"1%"}}>
                
                <ul style={{width:"98%",listStyleType: 'none'}}>

                    <li className={this.classes.infoItem}>

                    <Chip
                      avatar={<Avatar><PlusOneIcon /></Avatar>}
                      label="participants : 0"
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><SchoolIcon /></Avatar>}
                      label="gagnants : 0"
                    /></li>
                     <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><MoodBadIcon /></Avatar>}
                      label="perdants : 0"
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><HeadestMicIcon /></Avatar>}
                      label="animateur : borhen94"
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><PortraitIcon /></Avatar>}
                      label="editeur : omar94"
                    /></li>
                </ul>
                <br/>
                <div style={{right:0, marginLeft:"84%"}}>

                <IconButton size="small"  className={this.classes.button}>
                    <EditIcon className={this.classes.extendedIcon} />
                </IconButton>
                <IconButton  size="small"  className={this.classes.button} onClick={this.handleClickOpen.bind(this,itemData)}>
                    <DeleteIcon className={this.classes.extendedIcon} />
                </IconButton>
                </div>      
             
            </div>
        );else return (
          <div style={{marginLeft:"1%"}}>
                
                <ul style={{width:"98%",listStyleType: 'none'}}>

                    <li className={this.classes.infoItem}>

                    <Chip
                      avatar={<Avatar><PlusOneIcon /></Avatar>}
                      label={"participants :"+ (item.nb_loser+item.nb_winner)}
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><SchoolIcon /></Avatar>}
                      label={"gagnants :"+item.nb_winner}
                    /></li>
                     <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><MoodBadIcon /></Avatar>}
                      label={"perdants :"+item.nb_loser}
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><HeadestMicIcon /></Avatar>}
                      label="animateur : borhen94"
                    /></li>
                    <li className={this.classes.infoItem}><Chip
                      avatar={<Avatar><PortraitIcon /></Avatar>}
                      label="editeur : omar94"
                    /></li>
                </ul>
                     
            </div>
        );
    }
    render(){
     return(<div>
        <ReactTable
              
            data={this.state.data}
            className="-striped -highlight"
            columns={columns}
            SubComponent={this.renderSubComponent.bind(this)}
            onExpandedChange={this.handleExpand.bind(this)}
            defaultPageSize={5}
            getTrProps={renderRow}
            freezeWhenExpanded={false}
            expanded={this.state.expandedColumns}
            previousText='Précédent '
            nextText='Suivant'
            loadingText='en cours...'
            noDataText='vide'
            pageText='Page'
            ofText='de'
            rowsText='ligne'
            sortable= {false}
            page={this.state.page}
            onPageChange={(pageIndex) => {this.setState({page:pageIndex})}} 
     /> 
          <Button variant="fab" color="primary" aria-label="new session" className={this.classes.fabButton}
          onClick={() => {       this.props.history.push('/dashboard/timeline',{exp:"exp"});
        }}
          >
            <AddIcon />
          </Button>
          {this._renderDialog()}
        </div>
     ) 
    }
 }
 TableSession.propTypes = {
    classes: PropTypes.object.isRequired,

  };
function renderRow(state, rowInfo, column){
    if(rowInfo)return {
      style: {
        textAlign:'center',
        background: rowInfo.row.etat ===0 ? "lightpink" : "lightblue"
      }
    };else return {};
  }
export default withStyles(styles)(TableSession);
