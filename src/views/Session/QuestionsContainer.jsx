import React from "react"
import Paper from 'material-ui/Paper';
import Grid from "material-ui/Grid";
import PropTypes from "prop-types";
import QuestionInput from "./QuestionInput.jsx";
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import SaveIcon from 'material-ui-icons/Save';

import { withStyles } from "material-ui";
import axios from 'axios';
import globale from "../../config.js";
import styles from "../../variables/styles/questionContainerStyle.jsx";
import {Snackbar} from "components";
import { AddAlert } from "material-ui-icons";
class QuestionsContainer extends React.Component{
  componentDidMount(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.CATEGORIE_API+"/getALL";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} })
       .then(function (response) {
         self.setState({
           categories:response.data
         });
       });


  }
  constructor(props){
    super(props);
    this.classes = this.props.classes ;
    this.state = {
      questions: [

      ],
      etats:[],
      categories:[],
      valides:0,
      tr: false,
      content: "operation d insertion a echoue",
      color:"primary"
    }
  }
  showNotification(place) {
    var x = [];

    x[place] = true;
    this.setState(x);
    setTimeout(
      function() {
        x[place] = false;
        this.setState(x);
      }.bind(this),
      6000
    );
  }
  saveQuizz(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.QUESTION_API+"/saveALL";
    axios.post(api,{questions:this.state.questions},{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      if(response.status===200){
        //afficher notification & vider questions
        self.setState({
          color:"info",
          content: response.data.message,
          questions:[],
          etats:[],
          valides:0
        });
        self.showNotification("tr");
      }
    }).catch(function(error){
      self.setState({
        color:"warning",
        content: "operation d insertion a echoue"
      });
      self.showNotification("tr");

    })
  }
  addQuestion(event){
  let questions =this.state.questions.slice();
  let etats = this.state.etats.slice();
   let nv_question = {
      content: "new question",
      categorie: "categorie",
      propositions:[
        {content:"first prop",estCorrecte:false},
        {content:"second prop",estCorrecte:false},
        {content:"last prop",estCorrecte:false}
      ]
    }
    etats.push(0);
    questions.push(nv_question);
    this.setState({questions,etats});
  }
  countValide(bool,key){
      let etats = this.state.etats.slice();
      (bool===true)?etats[key]=1:etats[key]=0;
      let valides = etats.reduce(function(memo, val) {
          return memo + val;
          });
          this.setState({etats,valides});
  }
  handleSupprimer(key){
    let questions =this.state.questions.slice();
    let etats = this.state.etats.slice();
    var valide = etats[key];
    etats.splice(key,1);
    questions.splice(key,1);
    let valides = this.state.valides;
    if(valide === 1)valides--;
    this.setState({questions,etats,valides});
  }
    updateState = (key,question) => {
      let questions = this.state.questions.slice();
      questions[key]= Object.assign({},question);
      this.setState({questions},function(){
      });
    }
  render(){
    return (
        <Grid container>
        <Paper>
        {
          this.state.questions.map((question,key) => (
          <QuestionInput question={question} numero={key} supprimer={this.handleSupprimer.bind(this)}
          update={this.updateState.bind(this)} categories={this.state.categories}
          validation={this.countValide.bind(this)}
          />
           ))
         }
        </Paper>
        <Button variant="fab" color="primary" aria-label="add" className={this.classes.button}
        onClick={this.addQuestion.bind(this)}
        >
          <AddIcon />
        </Button>
          {
            ((this.state.valides === this.state.questions.length)&&(this.state.questions.length > 0))?
              <Button variant="fab" color="primary" aria-label="save" className={this.classes.secondButton}
              onClick={this.saveQuizz.bind(this)}
              >
                <SaveIcon />
              </Button>:

              <Button variant="fab" color="primary" aria-label="save" className={this.classes.secondButton}
              onClick={this.saveQuizz.bind(this)} disabled
              >
                <SaveIcon />
              </Button>

          }
          <Snackbar
            place="tr"
            color={this.state.color}
            icon={AddAlert}
            message={this.state.content}
            open={this.state.tr}
            closeNotification={() => this.setState({ tr: false })}
            close
          />


        </Grid>
    )
  }
}
QuestionsContainer.propTypes = {
  classes:PropTypes.object.isRequired
}
export default withStyles(styles)(QuestionsContainer);
