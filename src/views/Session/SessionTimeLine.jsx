import React from 'react';
import PropTypes from 'prop-types';
import { withStyles ,MuiThemeProvider, createMuiTheme} from 'material-ui/styles';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import QueueIcon from 'material-ui-icons/Queue';
import IconButton from 'material-ui/IconButton';
import Timer from "material-ui-icons/Timer";
import TextField from 'material-ui/TextField';
import lightGreen from 'material-ui/colors/lightGreen';
import lightBlue from 'material-ui/colors/lightBlue';
import Save from 'material-ui-icons/Save';
import ClearIcon from 'material-ui-icons/Clear';
import Help from 'material-ui-icons/Help';
import OfflinePin from 'material-ui-icons/OfflinePin';
import MovieCreation from "material-ui-icons/MovieCreation";
import classNames from 'classnames';
import FullScreenDialog from "components/login/Modal.jsx";
import Checkbox from 'material-ui/Checkbox';
import { FormControlLabel } from 'material-ui/Form';
import {ItemGrid} from "components";
import MenuItem from 'material-ui/Menu/MenuItem';
import {Snackbar} from "components";
import { AddAlert } from "material-ui-icons";
import axios from 'axios';
import globale from "../../config.js";

const theme = createMuiTheme({
  palette: {
    primary: lightGreen,
    secondary:lightBlue

  },
});

class SessionTimeLine extends React.Component{
constructor(props){
super(props);
this.classes = this.props.classes ;
this.state = {
  color:"warning",
  content: "operation d insertion a echoue",
 date_debut:"2018-05-24T10:30",
 cagnotte:0,
 right:false,estValide:false,
 levels:[],
 visibles:[],
 delegate:false,
 animateur:"",
 editeur:"",
 valides:[],
 tr:false,
 animateurs:[],
 editeurs:[]
}
};
handleChange = name => event => {
  this.setState({ [name]: event.target.value });
};

handleClickOpen= (name) => event => {
  this.setState({ [name]: true });
};

handleClose = () => {
  this.setState({ introVideo: false });
};
closeDialog(index,questions,estValide){
  let levels = this.state.levels.slice(0);
  let visibles = this.state.visibles.slice(0);
  let valides=this.state.valides.slice(0);
  visibles[index] = !visibles[index] ;
  levels[index].questions = questions;
  valides[index]=estValide;
   this.setState({visibles,levels,valides});
}
handleModal(index){
  let visibles = this.state.visibles.slice(0);
  visibles[index] = !visibles[index] ;
 this.setState({visibles},function(){
   console.log(this.state.levels);
 });
}
showNotification(place) {
  var x = [];
  x[place] = true;
  this.setState(x);
  setTimeout(
    function() {
      x[place] = false;
      this.setState(x);
    }.bind(this),
    6000
  );
}
addLevel(){
  let levels = this.state.levels.slice(0);
  let visibles =this.state.visibles.slice(0);
  let valides=this.state.valides.slice(0);
  valides.push(false);
  visibles.push(false);
  levels.push({num:levels.length+1,questions:[]});
  this.setState({levels,visibles,valides});
}
componentWillMount(){
  var self = this;
  var mytoken = localStorage.getItem("admin-token");
  let api=globale.backend_server.uri+  globale.API.LOGIN_API+"/editeurs";
  axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
    if(response.status===200){
      self.setState({editeurs:response.data});
    }
  }).catch(function(error){
    console.log(error);

  })
  api=globale.backend_server.uri+  globale.API.LOGIN_API+"/animateurs";
  axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
    if(response.status===200){
      self.setState({animateurs:response.data});
    }
  }).catch(function(error){
    console.log(error);
  })
}
deleteLevel(index){
  let levels = this.state.levels.slice(0);
  let visibles =this.state.visibles.slice(0);
  let valides=this.state.valides.slice(0);

  levels.splice(index,1);
  visibles.splice(index,1);
  valides.splice(index,1);
  this.setState({levels,visibles,valides});
}
saveQuizz(questions,callback){
  var mytoken = localStorage.getItem("admin-token");
  let api=globale.backend_server.uri+  globale.API.QUESTION_API+"/saveALL";
  axios.post(api,{questions:questions},{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
    if(response.status===200){
      var ids = Object.values(response.data.insertedIds);
      callback(ids);
    }
  }).catch(function(error){
    console.log(error);

  })
}
_renderTimeline(){
  if(this.state.delegate === true) return null;
  else {
  return(
  this.state.levels.map((value,index) =>
(index % 2 ===0)?<VerticalTimelineElement
className="vertical-timeline-element--work"
date={"Level "+(index+1)}
iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
icon={<Timer />}
>
<IconButton color="default" aria-label="clear" className={this.classes.buttonClear} onClick={
this.deleteLevel.bind(this,index)}>
    <ClearIcon />
  </IconButton>
<h3 className="vertical-timeline-element-title">Niveau de difficulté = 3 questions </h3>
<h4 className="vertical-timeline-element-subtitle">  </h4>
<MuiThemeProvider theme={theme}>
        <Button variant="raised" color="secondary" className={this.classes.whiteText}
         onClick={this.handleModal.bind(this,index)}>
        ouvrir l'editeur
        </Button>
  </MuiThemeProvider>
  <FullScreenDialog questions = {this.state.levels[index].questions } index={index} visible={this.state.visibles[index]} closer = {this.closeDialog.bind(this)}
  />

</VerticalTimelineElement>:
<VerticalTimelineElement
    className="vertical-timeline-element--work"
    date={"Level "+(index+1)}
    iconStyle={{ background: 'rgb(92, 244, 66)', color: '#fff' }}
    icon={<Timer />}
  >
  <IconButton color="default" aria-label="clear" className={this.classes.buttonClear} onClick={
this.deleteLevel.bind(this,index)}>
        <ClearIcon />
      </IconButton>
    <h3 className="vertical-timeline-element-title">Niveau de difficulté = 3 questions </h3>
    <h4 className="vertical-timeline-element-subtitle"> </h4>
    <MuiThemeProvider theme={theme}>
            <Button variant="raised" color="primary" className={this.classes.whiteText}
             onClick={this.handleModal.bind(this,index)}>
            ouvrir l'editeur
            </Button>
      </MuiThemeProvider>
      <FullScreenDialog questions = {this.state.levels[index].questions } index={index} visible={this.state.visibles[index]} closer = {this.closeDialog.bind(this)}
      />
  </VerticalTimelineElement>

))
}
}
_renderSave(){
  var debut_valide = false;
  var fin_valide = false;
  var filtred = this.state.valides.filter(valide => valide ===true);
  if(this.state.date_debut && this.state.cagnotte>0)debut_valide = true;
  if(this.state.animateur !== "" && this.state.editeur !== "")fin_valide = true;
  if(this.state.delegate && debut_valide && fin_valide) return(
    <Button
    onClick={this.handleSave.bind(this)}
  className={this.classes.button} variant="raised" size="small" color="primary"
 >
<Save className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
      Enregistrer
</Button>
  );else if (debut_valide && fin_valide && filtred.length === this.state.levels.length && filtred.length>0)return(
  <Button
  className={this.classes.button} variant="raised" size="small" color="primary"
  onClick={this.handleSave.bind(this)}
 >
<Save className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
      Enregistrer
</Button>);else return (<Button
  className={this.classes.button} variant="raised" size="small" color="primary"
 disabled>
<Save className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
      Enregistrer
</Button>)
}
handleSave(){
  console.log("*******************save***********************");

  var self=this;
  var mytoken = localStorage.getItem("admin-token");
  var questions = [];
  var levels = this.state.levels;
  for (var i=0;i<levels.length;i++){
    var current = levels[i];
    for (var j=0;j<current.questions.length;j++){
      current.questions[j].level = current.num;
      questions.push(current.questions[j]);
    }
  }
  this.saveQuizz(questions,function(ids){
    let api=globale.backend_server.uri+  globale.API.SESSION_API+"/addSession";
    axios.post(api,{session:{
      questions:ids,
      animateur:self.state.animateur,
      editeur:self.state.editeur,
      cagnotte:self.state.cagnotte,
      date_dem:self.state.date_debut
    }},{ headers: { Authorization: "Bearer "+ mytoken} })
       .then(function (response) {
        self.setState({
          tr:true,
          cagnotte:0,
          content:"la session est creee",
          date_dem:"0000",
          levels:[],
          visibles:[],
          valides:[],
          color:"primary"
        });
        self.showNotification("tr");
      }).catch(function(err){
        self.setState({
          color:"warning",
          content: "operation d insertion a echoue"
        });
        self.showNotification("tr");
      })
  })
}
render(){
  return (
  <div>
  <VerticalTimeline>
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    date="Debut"
    iconStyle={{ background: 'rgb(193, 8, 8)', color: 'white' }}
    icon={<MovieCreation />}

  >
    <h3 className="vertical-timeline-element-title">Information de base</h3>
               <TextField

                  id="margin-none"
                  type="datetime-local"
                  value={this.state.date_debut}
                  className={this.classes.textField}
                  onChange = {this.handleChange("date_debut").bind(this)}
                />
                 <TextField
                  label="cagnotte"
                  id="margin-none"
                  type="number"
                  value={this.state.cagnotte}

                  className={this.classes.textField}
                  onChange = {this.handleChange("cagnotte").bind(this)}
                />
                <br />
               <IconButton className={this.classes.status} color="primary" disabled>
      {(this.state.date_debut && this.state.cagnotte>0) ?<OfflinePin />:<Help />
       }
       </IconButton>
    </VerticalTimelineElement>
    <FormControlLabel
         style={{position:"relative",marginLeft:"47%",width:"120px",border:"dashed"}}
          control={
            <Checkbox
              checked={this.state.delegate}
              onChange={() => this.setState({delegate:!this.state.delegate})}
              value="delegate"
            />
          }
          label="delegate"
        />
{this._renderTimeline()}
   <VerticalTimelineElement
    className="vertical-timeline-element--work"
    date="Fin"
    iconStyle={{ background: 'rgb(193, 8, 8)', color: 'white' }}
    icon={<MovieCreation />}
  >
    <h4 className="vertical-timeline-element-subtitle">Affectation </h4>
    <Grid container >
    <ItemGrid xs={12} sm={12} md={12}>
        <TextField
                  id="select-animateur"
                  select
                  label="animateur"
                  className={this.classes.textField}
                  value={this.state.animateur}
                  onChange={this.handleChange('animateur')}
                  SelectProps={{
                    MenuProps: {
                      className: this.classes.menu,
                    }
                  }}
                  margin="normal"
                >
                  {this.state.animateurs.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
        </ItemGrid>
        <ItemGrid xs={12} sm={12} md={12}>
        <TextField
                  id="select-editeur"
                  select
                  label="editeur"
                  className={this.classes.textField}
                  value={this.state.editeur}
                  onChange={this.handleChange('editeur')}
                  SelectProps={{
                    MenuProps: {
                      className: this.classes.menu,
                    }
                  }}
                  margin="normal"
                >
                  {this.state.editeurs.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
        </ItemGrid>
        </Grid>
        <br />
        <IconButton className={this.classes.status} color="primary" disabled>
      {(this.state.animateur && this.state.editeur) ?<OfflinePin />:<Help />
       }
       </IconButton>
      </VerticalTimelineElement>
      {(this.state.delegate)?<IconButton disabled color="default" aria-label="clear" className={this.classes.buttonToolbox}
           onClick={this.addLevel.bind(this)} >
              <QueueIcon />
              </IconButton>:<IconButton  color="default" aria-label="clear" className={this.classes.buttonToolbox}
           onClick={this.addLevel.bind(this)} >
              <QueueIcon />
              </IconButton>}

</VerticalTimeline>
<Grid container>
<Grid xs={5} >
<Snackbar
  place="tr"
  color={this.state.color}
  icon={AddAlert}
  message={this.state.content}
  open={this.state.tr}
  closeNotification={() => this.setState({ tr: false })}
  close
/>
</Grid>
<Grid item xs={6}>
{this._renderSave()}

</Grid>
</Grid>
</div>
);
}
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  button:{

  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
  },
  container: {
   display: 'flex',
   flexWrap: 'wrap',
 },
 formControl: {
   margin: theme.spacing.unit,
   minWidth: 120,
 },
 textField: {
   marginLeft: theme.spacing.unit,
   marginRight: theme.spacing.unit,
   width: "80%",
 },
 whiteText:{
   color:'white'
 },
 blueButton:{
   backgroundColor:"",
   color:'white'
 },buttonClear:{
  // height:"5px",
   position:"absolute",
   right:"0"
 },
 buttonToolbox:{
  // height:"5px",
   position:"absolute",
   left:"48%",
   width:"40px",
   height:"40px",
   borderRadius:30,
   border:"dashed",
   margin:2
 },
 list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  card: {
    //height:"200px",
    width:'80%',
    marginLeft:'10%',
    marginBottom: '2%',
    marginTop:'4%',
    border:"1px solid #d6bdbd",
    borderRadius:'20px'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 12,
  },
  imgStep:{
    width:'100%',
    height:'80%'
  },
  status:{
  position:"absolute",
  right:0,
  top:0
  },
  dashed:{
    border:"dashed"
  }
});
SessionTimeLine.propTypes = {
  classes:PropTypes.object.isRequired
}
export default withStyles(styles)(SessionTimeLine);
