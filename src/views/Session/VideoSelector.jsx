import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Dialog, { DialogActions, DialogContent, DialogTitle } from 'material-ui/Dialog';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Select from 'material-ui/Select';
import Button from 'material-ui/Button';

class VideoSelector extends React.Component{
  constructor(props){
    super(props);
    this.classes = this.props.classes;
    this.state = {
      visible : this.props.visible,
      type : "video",
      ref:  ""        ,
      videos : this.props.videos,
      which:this.props.which
    }
  }

  handleChange= event => {
    this.setState({ref:event.target.value});
  }
  componentWillReceiveProps(props){
    this.setState({visible:props.visible,videos:props.videos});
  }
  close(){
    this.setState({visible:false},function(){
      this.props.closer(this.state.which,{type:"video",ref:this.state.ref});
    });
  }
  render(){
    return(
      <Dialog
         disableBackdropClick
         disableEscapeKeyDown
         open={this.state.visible}
         onClose={this.close.bind(this)}
        >
        <DialogTitle>Selecteur video</DialogTitle>
          <DialogContent>
            <form className={this.classes.container}>
              <FormControl className={this.classes.formControl}>
                <InputLabel htmlFor="age-native-simple">Titre</InputLabel>
                <Select
                  native
                  value={this.state.ref}
                  onChange={this.handleChange.bind(this)}
                  input={<Input id="age-native-simple" />}
                >
                  <option value="" />
                  <option value="12458">sessionone.mp4 </option>
                  <option value="11458">sessiontwo.mp4 </option>
                  <option value="12468">sessionthree.mp4 </option>
                </Select>
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.close.bind(this)} color="primary">
              Annuler
            </Button>
            <Button onClick={this.close.bind(this)} color="primary">
              Valider
            </Button>
          </DialogActions>
        </Dialog>
    )
  }
}
const style_video = theme => ({container: {
 display: 'flex',
 flexWrap: 'wrap',
},
formControl: {
 margin: theme.spacing.unit,
 minWidth: 120,
}});
VideoSelector.propTypes = {
  classes:PropTypes.object.isRequired,
  closer:PropTypes.func.isRequired,
  which:PropTypes.string.isRequired
}
export default withStyles(style_video)(VideoSelector);
