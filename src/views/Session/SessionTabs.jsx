import React from 'react';
import Paper from 'material-ui/Paper';
import Tabs, { Tab } from 'material-ui/Tabs';
import DonutSmall from 'material-ui-icons/DonutSmall';
import PlaylistAdd from 'material-ui-icons/PlaylistAdd';
import Typography from 'material-ui/Typography';
import PropTypes from "prop-types";
import EnhancedTable from "../tables/QuestionTable.jsx";
import Divider from "material-ui/Divider";
import TextField from 'material-ui/TextField';
import Button from "material-ui/Button";
import { withStyles } from "material-ui";
import axios from 'axios';
import globale from "../../config.js";
import Save from 'material-ui-icons/Save';
import classNames from 'classnames';
import {Snackbar} from "components";
import { AddAlert } from "material-ui-icons";
import tabsStyles from "../../variables/styles/sessionTabsStyle.jsx";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

class IconLabelTabs extends React.Component {
  constructor(props){
    super(props);
    this.classes = props.classes;
    this.state = {
      value: 0,
      cagnotte:10000,
      date_time:"2018-05-24T10:30",
      questions:[],
      nb_question:12,
      tr: false,
      content: "operation d insertion a echoue",
      color:"primary",
      visible:this.props.visible
    };
  }
  showNotification(place) {
    var x = [];
    x[place] = true;
    this.setState(x);
    setTimeout(
      function() {
        x[place] = false;
        this.setState(x);
      }.bind(this),
      6000
    );
  }
  updateQuestions = (questions) => {
    this.setState({questions});
  }
  handleChange = (event,value) => {
    this.setState({value });
  };
  handleChangement = (name) => event => {
    if(name === "nb_question"){
      this.setState({[name]: parseInt(event.target.value,10)});
    }
    else this.setState({[name]:event.target.value});
 }
  componentDidMount(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.QUESTION_API+"/getALL";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} })
       .then(function (response) {
          self.setState({
           data:response.data
         });
       });
  }
  saveSession=() => {
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.SESSION_API+"/addSession";
    axios.post(api,{session:{
      questions:this.state.questions,
      cagnotte:this.state.cagnotte,
      date_dem:this.state.date_time
    }},{ headers: { Authorization: "Bearer "+ mytoken} })
       .then(function (response) {
        self.setState({
          tr:true,
          cagnotte:0,
          content:"la session est creee",
          date_dem:"0000",
          data:[],
          color:"primary"
        });
        self.showNotification("tr");
      }).catch(function(err){
        self.setState({
          color:"warning",
          content: "operation d insertion a echoue"
        });
        self.showNotification("tr");
      })
  }
  render() {
    return(

      <Paper className={this.classes.dashed}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          fullWidth
          indicatorColor="red"
        >
          <Tab icon={<DonutSmall  />} label="Studio" />
          <Tab icon={<PlaylistAdd />} label="Liste des particpants" />
          <Tab icon={<PlaylistAdd />} label="liste des gagnants" />
          <Tab icon={<PlaylistAdd />} label="liste des cadeaux" />
        </Tabs>
        {this.state.value === 0 &&
          <TabContainer>
          <ul>
          <li>liste des sessions</li>
          <li>statistique de chaque session</li>
          </ul>
          </TabContainer>}
          {this.state.value === 1 &&
            <TabContainer>
            <ul>
            <li>liste des sessions</li>
            <li>statistique de chaque session</li>
            </ul>
            </TabContainer>}
            {this.state.value === 2&&
              <TabContainer>
              <ul>
              <li>liste des sessions</li>
              <li>statistique de chaque session</li>
              </ul>
              </TabContainer>}


        {this.state.value === 3&&
           <TabContainer>
            <Paper className={this.classes.dashed}>
            <form className={this.classes.form} noValidate autoComplete="off">
            <TextField
            id="datetime-local"
            label="date"
            type="datetime-local"
            value={this.state.date_time}
            onChange={this.handleChangement("date_time")}
            className={this.classes.textField}
            InputLabelProps={{
            shrink: true,
            }}
            />
            <TextField
            id="num"
            label="Cagnotte"
            value={this.state.cagnotte}
            onChange={this.handleChangement("cagnotte")}
            type="number"
            className={this.classes.textFieldCag}
            InputLabelProps={{
              shrink: true,
            }}
            margin="normal"
            />
            <TextField
            id="nb-question"
            label="nombre question"
            value={this.state.nb_question}
            onChange={this.handleChangement("nb_question")}
            type="number"
            className={this.classes.textFieldNb}
            InputLabelProps={{
              shrink: true,
            }}
            margin="normal"
            />
          </form>
            <Divider />
            <EnhancedTable data={this.state.data} selectedQuestions={this.updateQuestions.bind(this)}/>
            </Paper>
              {(this.state.questions.length === this.state.nb_question)?<Button
                className={this.classes.button} variant="raised" size="small" color="primary"
                onClick={this.saveSession.bind(this)}>
              <Save className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
                    Enregistrer
              </Button>:<Button className={this.classes.button} variant="raised" size="small" color="primary" disabled>
              <Save className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
                    Enregistrer
              </Button>}
              </TabContainer>}
              <Snackbar
                place="tr"
                color={this.state.color}
                icon={AddAlert}
                message={this.state.content}
                open={this.state.tr}
                closeNotification={() => this.setState({ tr: false })}
                close
              />
      </Paper>
    )
  }
}
IconLabelTabs.propTypes = {
  classes:PropTypes.object.isRequired,
  visible:PropTypes.bool.isRequired
}
export default withStyles(tabsStyles)(IconLabelTabs);
