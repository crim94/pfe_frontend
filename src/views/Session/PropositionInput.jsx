import React from "react";
import PropTypes from "prop-types";
import classNames from 'classnames';
import {ItemGrid} from "components";
import IconButton from 'material-ui/IconButton';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import { withStyles } from "material-ui";
import CheckCircle from 'material-ui-icons/CheckCircle';
import NotInterested from 'material-ui-icons/NotInterested';
const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width:"100%",
  },
  menu: {
    width: 200,
  },
});
class PropositionInput extends React.Component {
  constructor(props){
    super(props);
    this.classes=props.classes;
    this.state = {
      proposition:props.proposition,
      index: props.index
    }
  }
  componentWillReceiveProps(props){
  let proposition = props.proposition;
  let index = props.index;
  this.setState({proposition,index},function(){
  })
  }
  handleCheckProposition(){
    let proposition = Object.assign({}, this.state.proposition);
    proposition.estCorrecte=!proposition.estCorrecte;
    this.props.checker(this.state.index);
    this.setState({proposition});
  }
  handleChange = (name) => event => {
    let proposition = Object.assign({}, this.state.proposition);
    proposition[name] = event.target.value;
    this.setState({proposition});
    this.props.updateProposition(event.target.value,this.state.index);
  }
  render(){
    return (
      <ItemGrid xs={12} sm={12} md={12}>
      <FormControl className={classNames(this.classes.margin, this.classes.textField)}>
      <InputLabel htmlFor="proposition-one">proposition</InputLabel>
      <Input
       id="input-prop"
       type="text"
       value={this.state.proposition.content}
       onChange={this.handleChange('content')}
       endAdornment={
         <InputAdornment position="end">
           <IconButton
             aria-label="label"
             onClick={this.handleCheckProposition.bind(this)}
             >
             {this.state.proposition.estCorrecte ?  <CheckCircle />:<NotInterested />}
           </IconButton>
         </InputAdornment>
       }
       />
      </FormControl>
      </ItemGrid>

    )
  }
}
PropositionInput.propTypes = {
  proposition:PropTypes.object.isRequired,
  classes:PropTypes.object.isRequired,
  index:PropTypes.number.isRequired,
  checker:PropTypes.func.isRequired,
  updateProposition:PropTypes.func.isRequired
}
export default withStyles(styles)(PropositionInput);
