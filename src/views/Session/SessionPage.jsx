import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import List, {
  ListItem,
  ListItemAvatar,

  ListItemSecondaryAction,
  ListItemText,
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';


import Grid from 'material-ui/Grid';

import ModeEdit  from 'material-ui-icons/ModeEdit'
//import IconLabelTabs from "views/Session/SessionTabs.jsx";
const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
  },
});

function generate(element) {
  return [0, 1, 2,3,4,5].map(value =>
    React.cloneElement(element, {
      key: value,
    }),
  );
}
class SessionPage extends React.Component {
  state = {
    dense: false,
    secondary: false,
    isVisible:false
  };
  clickEdit(){
    this.setState({isVisible:true});
  }
  render() {
    const { classes } = this.props;
    const { dense, secondary } = this.state;
    return (
      <div className={classes.root}>
          <Grid item xs={12} md={12}>
            <div className={classes.demo}>
              <List dense={dense}>
                {generate(
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar>
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary="session "
                      secondary={secondary ? 'Secondary text' : null}
                    />
                    <ListItemSecondaryAction>
                      <IconButton aria-label="Delete" onClick={this.clickEdit.bind(this)}>
                        <ModeEdit />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                )}
              </List>
            </div>
          </Grid>
          <Grid>

          </Grid>
      </div>
    );
  }
}

SessionPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SessionPage);
