import React from "react";
import Grid from "material-ui/Grid";
import {ItemGrid} from "components";
import PropTypes from "prop-types";
import Typography from 'material-ui/Typography';
import { withStyles } from "material-ui";
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Divider from "material-ui/Divider";
import Help from 'material-ui-icons/Help';
import OfflinePin from 'material-ui-icons/OfflinePin';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ExpansionPanelActions,
} from 'material-ui/ExpansionPanel';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import PropositionInput from "./PropositionInput.jsx";
import styles from "../../variables/styles/questionInputStyle.jsx";


class QuestionInput extends React.Component {

  constructor(props){
    super(props);
    this.classes=props.classes;
    this.state = {
      question:this.props.question,
      index:this.props.numero,
      estValide:false
    }
  }
componentWillReceiveProps(props){
  var question = Object.assign({},props.question);
  var index=props.numero;
  var nb_checked = 0;
  for(var i = 0 ; i<3 ;i++)
      if(question.propositions[i].estCorrecte)nb_checked++;
  if(nb_checked === 1) this.setState({question:question,index:index,estValide:true});
  else this.setState({question,index});
}
  handleCheckProposition(key){
    let question=Object.assign({},this.state.question);
    question.propositions[key].estCorrecte = ! question.propositions[key].estCorrecte;
    this.setState({question},function(){
      var nb_checked = 0;
      for(var i = 0 ; i<3 ;i++)
          if(this.state.question.propositions[i].estCorrecte)nb_checked++;
      if(nb_checked !== 1){this.setState({estValide:false});
    this.props.validation(false,this.state.index);}
      else {this.setState({estValide:true});
        this.props.validation(true,this.state.index);
        }
      this.props.update(this.state.index,this.state.question);
    });
      }
  handleChange = (name) => event => {
    let question = Object.assign({}, this.state.question);
      question[name] = event.target.value;
    this.setState({question},function(){
      this.props.update(this.state.index,this.state.question);
    });
  };
  updateProposition=(value,index) => {
    let question = Object.assign({}, this.state.question);
      question.propositions[index].content = value;
    this.setState({question},function(){
    this.props.update(this.state.index,this.state.question);
    });
  };
  handleSupprimer(){
    this.props.supprimer(this.state.index);
  }
  render(){
    return (
      <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>Question numero # {this.props.numero} </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
      <Grid container>
        <ItemGrid xs={12} sm={12} md={12}>
        <TextField
        id="content-question"
        label="contenu"
        multiline
        rowsMax="4"
        value={this.state.question.content}
        onChange={this.handleChange('content')}
        className={this.classes.textField}
        margin="normal"
        />
        </ItemGrid>
        <ItemGrid xs={12} sm={12} md={12}>
        <TextField
                  id="select-category"
                  select
                  label="categorie"
                  className={this.classes.textField}
                  value={this.state.question.categorie}
                  onChange={this.handleChange('categorie')}
                  SelectProps={{
                    MenuProps: {
                      className: this.classes.menu,
                    }
                  }}
                  margin="normal"
                >
                  {this.props.categories.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
        </ItemGrid>
        <Divider light />

        {
          this.state.question.propositions.map((proposition, key) => (
          <PropositionInput proposition={proposition} index={key} checker={this.handleCheckProposition.bind(this)}
           updateProposition={this.updateProposition.bind(this)}/>
           ))
         }



        </Grid>
      </ExpansionPanelDetails>
      <Divider />
      <ExpansionPanelActions>
      <IconButton className={this.classes.status} color="primary" disabled>
      {this.state.estValide ?<OfflinePin />:<Help />
       }
       </IconButton>

        <IconButton className={this.classes.button} aria-label="Delete" color="secondary" onClick={this.handleSupprimer.bind(this)}>
           <DeleteIcon />
         </IconButton>
        </ExpansionPanelActions>
      </ExpansionPanel>


    )
  }

}

QuestionInput.propTypes = {
  classes: PropTypes.object.isRequired,
  question:PropTypes.object.isRequired,
  numero:PropTypes.number.isRequired,
  supprimer:PropTypes.func.isRequired,
  update:PropTypes.func.isRequired,
  categories:PropTypes.array.isRequired,
  validation:PropTypes.func.isRequired
};
export default withStyles(styles)(QuestionInput);
