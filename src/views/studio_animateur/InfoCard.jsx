import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Card,
    CardContent,
    CardActions
  } from "material-ui";
import Favorite from "material-ui-icons/Favorite";
import AttachMoney from "material-ui-icons/AttachMoney"
import { IconButton } from 'material-ui';
import ReactCountdownClock from "react-countdown-clock";
import AnimatorSteppers from "./AnimatorSteppers";
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import Event from "material-ui-icons/Event";
import socketIOClient from 'socket.io-client'


const styles = {
  card: {
    minWidth: 275,
    borderRadius:"10px",
    height:"450px"
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  proposition:{height:"33%",textAlign:"center",border:"solid 0.5px gray",
  borderRadius:"10px",marginBottom:"5px",
  lineHeight:3.5,backgroundColor:"oldlace"}
};
let socket = null;
let container = null;
class InfoCard extends React.Component {
constructor(props){
    super(props);
    container = this;
    this.classes = props.classes;
    this.state = {
      etat:"nextQuestion",
      questions:[{
        content:"Combien de fois la Tunisie a particip� dans le coupe du monde ?",
        categorie:"sport",
        propositions:["3","4","5"]
      }],
      result:["50","40","10"],
  
        cagnotte:0,
        viewers:0,
      currentIndex:0,
      nbsec:10,
      canSend:true,
      canMove:false //disable and enable next in steppers , the editor cannot move to next question only when current question sended
    }
}
componentWillMount(){
  socket = socketIOClient('http://localhost:4070');
  if(socket){
    socket.emit("animateurIn",{animateur:"ok"});
  }else console.log("error")

 socket.on('message', message => { 
   console.log(message);
   handleMessage(message);
 });
}

_renderBody(){ 
 
  let current = this.state.questions[this.state.currentIndex];
  if(current == undefined)return null;
  if(this.state.etat === "nextQuestion" || this.state.etat === "start"){
    return(
<div style={{height:"80%"}}>
          
          <div style={{height:"40%"}}>
          <div style={{height:"50%",paddingLeft:"40%",marginTop:"2px"}}> 
        {this.state.etat === "start"?<ReactCountdownClock seconds={this.state.nbsec}
                  color="red"
                  alpha={0.9}
                  size={60}
                  onComplete={()=> this.setState({nbsec:0})}
                  />:null}
           </div>
          <div style={{height:"50%",textAlign:"center",fontFamily:"andalus"}}>
          {current.content}
          </div>
          </div>
          <div style={{height:"60%"}}>
           <div style={{height:"180px"}}>
             {current.propositions.map((value,key) => (
             <div className={this.classes.proposition}>{value}</div>
             ))}
           </div>
          </div>
        </div>
    )
  }else {
    return(
<div style={{height:"80%"}}>
          
  <div style={{height:"40%"}}>
  <div style={{height:"50%",paddingLeft:"40%",marginTop:"4px"}}> 
  <Chip
      avatar={
        <Avatar><Event/></Avatar>
      }
        label="Resultat"

      />
   </div>
  <div style={{height:"50%",fontFamily:"andalus"}}>
  {current.content}
  </div>
  </div>
  <div style={{height:"60%"}}>
   <div style={{height:"180px"}}>
   {this.state.result.map((value,key) => (
    <div className={this.classes.proposition} style={{display: "-webkit-box",
     backgroundColor:"snow"}}>
     <div style={{width:"50%",textAlign: "left",
     paddingLeft: "30px"}}>{current.propositions[key].content}</div>
     <div style={{width:"50%"}}>{value}%</div>
     </div> 

   ))}
     
   </div>
  </div>
</div>)
  }
  
}
handleNext(){
  this.setState({currentIndex:this.state.currentIndex + 1,nbsec:10,canMove:false,etat:"nextQuestion"});
}
render(){
  return (
    <div>
      <Card className={this.classes.card}>
        <CardContent>
        <div style={{height:"20%",width:"100%",flex:1,alignContent:"center",alignItems:"center",borderBottom:"dashed 0.5px silver"}}>
            <IconButton disabled style={{width:"50%"}}>{this.state.viewers} <Favorite /> </IconButton>
            <IconButton disabled style={{width:"50%"}}>{this.state.cagnotte} <AttachMoney /> </IconButton>
         </div>
         {this._renderBody()}
         </CardContent>
        <CardActions style={{height:"20%"}}>
        
         <AnimatorSteppers disabled={true} activeStep={this.state.currentIndex} steps={this.state.questions.length}/>
        </CardActions>
      </Card>
    </div>
  );
}}

InfoCard.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(InfoCard);
function handleMessage(message){
  switch(message.id) {
    case "quiz":console.log(message);container.setState({questions:message.quiz,cagnotte:message.cagnotte,viewers:
    message.viewers,currentIndex:message.step});break;
    case "update":container.setState({viewers:message.watchers});break;
    case "result":var step = message.step;
    if(step === container.state.questions.length)step--;
    if(message.mode === 'a') container.setState({canSend:true,currentIndex:step});
    else container.setState({canSend:true}); break;
    case 'newQuestion':
    container.setState({etat:"start"});break;
    case "prepareYourSelf":container.setState({currentIndex:message.index,etat:"nextQuestion"});break;
    default:console.log("message non reconnu");
  }
}
