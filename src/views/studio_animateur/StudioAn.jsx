import React from "react"
import { withStyles,Grid } from "material-ui";
import Iframe from 'react-iframe';
import {ItemGrid} from "components";
import InfoCard from "./InfoCard";
import KurrentoWrapper from "../wrappers/KurrentoWrapper";
import {Broadcaster} from "components";
const styles = {
  flex: {
    flex: 1,
  }
};

class StudioAn extends React.Component{
  render() {
    return(
       <Grid container>
        <ItemGrid xs={7} sm={6} md={7} >
      <Broadcaster />
       </ItemGrid>
       <ItemGrid xs={5} sm={6} md={5} >
       <div style={{marginTop:"2em",width:"100%",height:"500px"}}>
       <InfoCard />
       </div>
       </ItemGrid>
       </Grid>
    );
  }
};
export default withStyles(styles)(StudioAn);
