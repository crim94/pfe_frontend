import React from "react"
import PropTypes from "prop-types";
import { withStyles,Grid} from "material-ui";
import {ItemGrid} from "components";
import socketIOClient from 'socket.io-client'
import Button from 'material-ui/Button';
import PlayArrow from 'material-ui-icons/PlayArrow';
import classNames from 'classnames';
const styles = {
  flex: {
    flex: 1,
  },
  button:{
    marginLeft:"44%",
    marginBottom:"2em"
  }
}
let socket = null;
let participants = {};
let name=null;
class MediaThequeContainer extends React.Component{
  constructor(props){
    super(props);
    this.classes = props.classes;
    this.state = {

    }
  }
  componentWillMount(){
     socket = socketIOClient('http://192.168.10.167:8443');
     if(socket)console.log("connected to streaming server ");
     socket.on('connect', () => {
      console.log('ws connect success');
    });
    socket.on('message', parsedMessage => { 
      handleMessage(parsedMessage);
    });


  }
  render(){
    return(
      <div>
        
        <Grid container>
        <ItemGrid xs={12} sm={12} md={12} >
        <Button
          className={this.classes.button} variant="raised" size="small" color="primary"
        >
        <PlayArrow className={classNames(this.classes.leftIcon, this.classes.iconSmall)} />
              commencer
        </Button>      
        </ItemGrid>
        <ItemGrid xs={7} sm={6} md={7} >
        <div style={{border:"dashed",width:"100%",height:"500px"}}>
        videoPanel
        </div>
        </ItemGrid>
        <ItemGrid xs={5} sm={6} md={5} >
        <div style={{border:"dashed",width:"100%",height:"500px"}}>
        infoPanel
        </div>
        </ItemGrid>
        </Grid>
      </div>
    )
  }
}
/**
 * 
 * @param {Object} message 
 * this function handle ws message from streaming server
 */
function handleMessage(parsedMessage){
  console.info('Received message: ' + parsedMessage.id);

	switch (parsedMessage.id) {
	case 'existingParticipants':
		onExistingParticipants(parsedMessage);
		break;
	case 'newParticipantArrived':
		onNewParticipant(parsedMessage);
		break;
	case 'participantLeft':
		onParticipantLeft(parsedMessage);
		break;
	case 'receiveVideoAnswer':
		receiveVideoResponse(parsedMessage);
		break;
	case 'iceCandidate':
		participants[parsedMessage.name].rtcPeer.addIceCandidate(parsedMessage.candidate, function(error) {
	        if (error) {
		      console.error("Error adding candidate: " + error);
		      return;
	        }
	    });
	    break;
	default:
		console.error('Unrecognized message', parsedMessage);
	}

}
function register() {
	name = "animateur"
	var roomName = "testRoom";

	var message = {
		id : 'joinRoom',
		name : name,
		roomName : roomName,
		whoAmi:'animateur'
	}
	sendMessage(message);
}

function onNewParticipant(request) {
	receiveVideo(request.name);
}

function receiveVideoResponse(result) {
	participants[result.name].rtcPeer.processAnswer (result.sdpAnswer, function (error) {
		if (error) return console.error (error);
	});
}

function callResponse(message) {
	if (message.response != 'accepted') {
		console.info('Call not accepted by peer. Closing call');
	} else {
		webRtcPeer.processAnswer(message.sdpAnswer, function (error) {
			if (error) return console.error (error);
		});
	}
}

function onExistingParticipants(msg) {
	var constraints = {
		audio : true,
		video : {
			mandatory : {
				maxWidth : 320,
				maxFrameRate : 15,
				minFrameRate : 15
			}
		}
	};
	var participant = new Participant(name);
	participants[name] = participant;
	var video = participant.getVideoElement();

	var options = {
	      localVideo: video,
	      mediaConstraints: constraints,
	      onicecandidate: participant.onIceCandidate.bind(participant)
	}
	participant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options,
		function (error) {
		  if(error) {
			  return console.error(error);
		  }
		  this.generateOffer(participant.offerToReceiveVideo.bind(participant));
	});

	msg.data.forEach(receiveVideo);
}

function leaveRoom() {
	sendMessage({
		'id': 'leaveRoom'
	});

	for (var key in participants) {
		participants[key].dispose();
	}

	document.getElementById('join').style.display = 'block';
	document.getElementById('room').style.display = 'none';

	socket.close();
}

function receiveVideo(sender) {
	var participant = new Participant(sender);
	participants[sender] = participant;
	var video = participant.getVideoElement();

	var options = {
      remoteVideo: video,
      onicecandidate: participant.onIceCandidate.bind(participant)
    }

	participant.rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options,
		function (error) {
			if(error) {
				return console.error(error);
			}
			this.generateOffer(participant.offerToReceiveVideo.bind(participant));
		}
	);
}

function onParticipantLeft(request) {
	console.log('Participant ' + request.name + ' left');
	var participant = participants[request.name];
	participant.dispose();
	delete participants[request.name];
}

function sendMessage(message) {
	console.log('Senging message: ' + message.id);
	socket.emit('message', message);
}


MediaThequeContainer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MediaThequeContainer);
