import React from "react";
import PropTypes from "prop-types";
import {
  DateRange,
  Update,
  VideogameAsset,
  Stars,InsertInvitation,PersonAdd

} from "material-ui-icons";
import { withStyles, Grid } from "material-ui";

import {
  StatsCard,
  ItemGrid,RegularCard,
  TasksCard
} from "components";
import { BarChart,XAxis,YAxis,Bar,Legend,Tooltip,CartesianGrid,
  PieChart, Pie, Sector,Area,AreaChart} from 'recharts';


import dashboardStyle from "variables/styles/dashboardStyle";
import axios from 'axios';
import globale from "../../config.js";
let container = null;
class Dashboard extends React.Component {
  constructor(props){
    super(props);
    container = this;
    this.state = {
      value: 0,
      maxParticipants:0,
       datapie :[{name: 'Culture', value: 400}, {name: 'Islamique', value: 300},
                  {name: 'Sport', value: 300}, {name: 'science', value: 200},
                  {name: 'autre', value: 500}],
      dataparsession:[],
      data_win_los:[],
      bestPlayer:{

      },
      nouvelleInscription:0,
      nextSession:{
        cagnotte:0,
        monthDay:"20/06",
        temps:"20:15"
      },
      dataSemaine:[{semaine: '24/2018',inscription: 10},
      {semaine: '25/2018', inscription: 5},
      {semaine: '26/2018', inscription: 15},
      {semaine: '27/2018', inscription: 15}]
    };
  }
  componentWillMount(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+  globale.API.STATISTIC+"/max";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      if(response.status===200){
        //afficher notification & vider questions
      self.setState({maxParticipants:response.data});
      }
    }).catch(function(error){
      console.log(error);
    });
    // pie chart 
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/parcategorie";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      self.setState({datapie:response.data});
    }).catch(function(error){
      console.log(error);
    })
    //participant par session
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/participantParSession";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
     self.setState({dataparsession:response.data});
    }).catch(function(error){
      console.log(error);
    })
    //winner and loser par session
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/winnerAndLoser";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
     self.setState({data_win_los:response.data});
    }).catch(function(error){
      console.log(error);
    })
    //bestPlayer
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/bestPlayer";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      response.data.avatar_uri =globale.backend_server.uri+ response.data.avatar_uri;
     self.setState({bestPlayer:response.data});
    }).catch(function(error){
      console.log(error);
    })
    //next session
    api=globale.backend_server.uri+  globale.API.SESSION_API+"/nextSession";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
     if(response.data.cagnotte)self.setState({nextSession:response.data});
    }).catch(function(error){
      console.log(error);
    })
    //nouvelle inscription
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/nbInscription";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      self.setState({nouvelleInscription:response.data.nb});
    }).catch(function(error){
      console.log(error);
    })
    //inscription per week
    api=globale.backend_server.uri+  globale.API.STATISTIC+"/inscriptionPerWeek";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      self.setState({dataSemaine:response.data});
    }).catch(function(error){
      console.log(error);
    })
  }
  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    return (
      <div>
        <Grid container>
        <ItemGrid xs={12} sm={6} md={3}>
          <StatsCard
            icon={VideogameAsset}
            iconColor="purple"
            title="Participations"
            description={"+"+this.state.maxParticipants}
            statIcon={Update}
            statText="nombre de participants"
          />
        </ItemGrid>
        <ItemGrid xs={12} sm={6} md={3}>
          <StatsCard
              icon={PersonAdd}
              iconColor="blue"
              description={this.state.nouvelleInscription}
              title="inscription"
              statIcon={DateRange}
              statText="nouvelle inscription"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
            <StatsCard
              icon={Stars}
              iconColor="red"
              description={this.state.bestPlayer.profit_tot}
              title={this.state.bestPlayer.username}
              statIcon={DateRange}
              statText="Meilleur joueur"
            />
          </ItemGrid>
          <ItemGrid xs={12} sm={6} md={3}>
          <StatsCard
              icon={InsertInvitation}
              iconColor="green"
              description={this.state.nextSession.cagnotte}
              title={this.state.nextSession.monthDay + " à "+ this.state.nextSession.temps}
              statIcon={DateRange}
              statText="prochaine session"
            />
          </ItemGrid>
        </Grid>
        <Grid container>
        <ItemGrid xs={12} md={6} >
        <RegularCard
         headerColor="purple"
         cardTitle="nomber de participants par session"
         content={
        <BarChart width={450} height={300} data={this.state.dataparsession}>
        <XAxis dataKey="date"/>
        <YAxis />
        <Tooltip/>
       <Legend />
        <Bar type="monotone" dataKey="nb" barSize={30} fill="purple"
          />
         </BarChart> }
         />
        </ItemGrid>
        <ItemGrid xs={12} md={6} >
        <RegularCard
         headerColor="purple"
         cardTitle="nomber de gagnants/perdants par session"
         content={
        <BarChart width={450} height={300} data={this.state.data_win_los}>
               <CartesianGrid strokeDasharray="3 3"/>

        <XAxis dataKey="name"/>
        <YAxis />
        <Tooltip/>
       <Legend />
        <Bar type="monotone" stackId="a" dataKey="nb_winner" barSize={30} fill="lightgreen"
          />
        <Bar type="monotone" stackId="a" dataKey="nb_loser" barSize={30} fill="orange"
          />
         </BarChart> }
         />
        </ItemGrid>
        <ItemGrid xs={12} md={6} >
        <RegularCard
         headerColor="purple"
         cardTitle="nomber d'elemination par categorie de question"
         content={ <TwoLevelPieChart /> }
         />
        </ItemGrid>
        <ItemGrid xs={12} md={6}>
        <RegularCard
          headerColor="purple"
          cardTitle="nombre d'inscription par semaine"
          content={
          <AreaChart width={450} height={300} data={this.state.dataSemaine}
          margin={{top: 10, right: 30, left: 0, bottom: 0}}>
          <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="semaine"/>
        <YAxis/>
      <Tooltip/>
      <Legend />

      <Area type='monotone' dataKey='inscription' stroke='#8884d8' fill='purple' />
    </AreaChart>} />
        </ItemGrid>
        <ItemGrid xs={12} md={6} >
        <TasksCard />
        </ItemGrid>
       
       
        </Grid>
        </div>
    );
  }
}

                 
const renderActiveShape = (props) => {
  const RADIAN = Math.PI / 180;
  const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
    fill, payload, percent, value } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 22;
  const ey = my;
  const textAnchor = cos >= 0 ? 'start' : 'end';

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Elemination ${value}`}</text>
      <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
        {`(Taux ${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  );
};

class TwoLevelPieChart extends React.Component{
	constructor(props){
    super(props);
    this.state = {
      activeIndex:0
    }
  }

  onPieEnter(data, index) {
    this.setState({
      activeIndex: index,
    });
  };
	render () {
  	return (
    	<PieChart width={800} height={400}>
        <Pie 
        	activeIndex={this.state.activeIndex}
          activeShape={renderActiveShape} 
          data={container.state.datapie} 
          cx={250} 
          cy={200} 
          innerRadius={100}
          outerRadius={125} 
          fill="purple"
          onMouseEnter={this.onPieEnter.bind(this)}
        />
       </PieChart>
    );
  }
};
Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
