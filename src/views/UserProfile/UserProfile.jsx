import React from "react";
import { Grid, withStyles } from "material-ui";
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import PropTypes from "prop-types";
import TextField from 'material-ui/TextField';
import { AddAlert } from "material-ui-icons";
import axios from 'axios';
import globale from "../../config.js";
import {
  ProfileCard,
  RegularCard,
  Button,
  ItemGrid
} from "components";
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  ExpansionPanelActions,
} from 'material-ui/ExpansionPanel';
import { MenuItem } from 'material-ui/Menu';
import {Snackbar} from "components";
import { CircularProgress } from 'material-ui/Progress';
import red from 'material-ui/colors/red';
import profileStyles from "../../variables/styles/userProfileStyle.jsx";

const roles =[
   {
     value: 'editeur',
     label: 'editeur'
   },
   {
     value: 'animateur',
     label: 'animateur'
   },
 ];
class UserProfile extends React.Component{
  constructor(props){
    super(props);
    this.classes = this.props.classes;
    var username="";
    var nom="";
    var prenom="";
    var avatar="";
    var role ="";
    var token = localStorage.getItem("admin-token");
    if(token)console.log(this.parseJwt(token));
    if(localStorage.getItem("user"))
      {username = (JSON.parse(localStorage.user)).username;
        nom = (JSON.parse(localStorage.user)).nom ;
        prenom = (JSON.parse(localStorage.user)).prenom ;
        avatar =(JSON.parse(localStorage.user)).avatar;
        role =(JSON.parse(localStorage.user)).role;
        }
    this.state = {
      users:[],
      tr: false,
      content: "msg",
      color:"primary",
      id:(JSON.parse(localStorage.user))._id,
      currentUser:{username: username,
      pswd:"",
      pswd2:"",
      avatar:avatar,
      nom:nom,
      role:role,
      prenom:prenom},
      loading: false,
      newUser:{
        username:"",
        pswd:"",
        password2:"",
        role:""
      },
      updateUser:{
        username:"",
        role:""
      }
    }
  }
  changeAvatar=(event) => {
    let self = this ;
    this.setState({loading:true});
    var mytoken = localStorage.getItem("admin-token");
    let formData = new FormData();
    formData.append('selectedFile', event.target.files[0]);
    formData.append("username",this.state.currentUser.username);
    var uri = globale.backend_server.uri + globale.API.LOGIN_API+"/upload";
    axios.post(uri, formData,{ headers: { Authorization: "Bearer "+ mytoken} })
         .then((response) => {
           var user=JSON.stringify(response.data);
           localStorage.setItem("user",user);
           let currentUser = Object.assign({},self.state.currentUser);
           currentUser.avatar = response.data.avatar;
           self.setState({currentUser:currentUser,loading:false},function(){
            });
         });
  }

  showNotification(place) {
    var x = [];
    x[place] = true;
    this.setState(x);
    setTimeout(
      function() {
        x[place] = false;
        this.setState(x);
      }.bind(this),
      6000
    );
  }
  parseJwt (token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse(window.atob(base64));
        };
  addUser(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+ globale.API.LOGIN_API+"/addAdmin";
    var payload = this.state.newUser;
    delete payload.password2;
    axios.post(api,{admin:payload},{ headers: { Authorization: "Bearer "+ mytoken} })
    .then(function(response){
      var newUser = {
      username:"",
      pswd:"",
      password2:"",
      role:""}
      self.setState({newUser:newUser,
        color:"info",
        content: "operation d ajout a effectue avec success"
      });
      self.showNotification("tr");
    }).catch(function(err){
      self.setState({
        color:"primary",
        content: "essayez de nouveau svp"
      });
      self.showNotification("tr");
    })
  }
  handleEdit(){
    var admin = this.state.currentUser;
    delete admin.pswd2;
    var payload = {admin:admin,
    id:this.state.id }
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+ globale.API.LOGIN_API+"/updateAdmin";
    axios.put(api,payload,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      var user=JSON.stringify(response.data.user);
      localStorage.setItem("user",user);
      let currentUser = Object.assign({},self.state.currentUser);
      currentUser.pswd2 = currentUser.pswd = "";
      currentUser.nom=response.data.user.nom;
      currentUser.prenom=response.data.user.prenom;
      self.setState({currentUser:currentUser,
        color:"info",
        content: "votre compte a ete modifier"
      });
      self.showNotification("tr");
    }).catch(function(err){
      self.setState({
        color:"primary",
        content: "essayez de nouveau"
      });
      self.showNotification("tr");
    });
  }
  handleChange = (name,sname) => (event) => {
    let user = Object.assign({},this.state[name]);
    user[sname]=event.target.value;
    if((name ==="updateUser")&&(sname === "username")){
      var retour = (this.state.users.filter(elt => elt.username===event.target.value))[0];
      user.role = retour.role;
    }
    this.setState({ [name]:user});

  };
  componentDidMount(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");

    let api=globale.backend_server.uri+ globale.API.LOGIN_API+"/all?current="+this.state.currentUser.username;
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
    if(response.status === 200) {let users = response.data;self.setState({users});

    }
  });

  }
  handleUpdate(){

    var self=this;
    var mytoken = localStorage.getItem("admin-token");

    let api=globale.backend_server.uri+ globale.API.LOGIN_API+"/updateRole";
    let payload = {user:this.state.updateUser};
    axios.put(api,payload,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
    if(response.status === 200) {let updateUser = Object.assign({},self.state.updateUser);
    updateUser.username = "";
    updateUser.role = "";
    self.setState({updateUser:updateUser,
      color:"info",
      content: "operation d update a effectue avec success"
    });
    self.showNotification("tr");
    }
  });
  };
  render(){
  return (
    <div>
      <Grid container>
        <ItemGrid xs={12} sm={12} md={8}>
          <RegularCard
            cardTitle="Modifier votre profile"
            className="redam"
            content={
              <div>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={12}>
                  <TextField
                    label="nom utilisateur"
                    id="edit-username"
                    disabled
                    value={this.state.currentUser.username}
                    className={this.classes.textField}
                  />
                  </ItemGrid>
                </Grid>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <TextField
                      label="nom"
                      id="ed-nom"
                      className={this.classes.textField}
                      onChange={this.handleChange("currentUser","nom")}
                        value={this.state.currentUser.nom}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <TextField
                      label="prenom"
                      id="ed-prenom"
                      className={this.classes.textField}
                      onChange={this.handleChange("currentUser","prenom")}
                        value={this.state.currentUser.prenom}

                    />
                  </ItemGrid>

                </Grid>
                <Grid container>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <TextField
                      label="mots de passe"
                      id="ed-pswd"
                      type="password"
                      className={this.classes.textField}
                      onChange={this.handleChange("currentUser","pswd")}
                      value={this.state.currentUser.pswd}
                    />
                  </ItemGrid>
                  <ItemGrid xs={12} sm={12} md={6}>
                    <TextField
                      label="confirmer"
                      type="password"
                      id="ed-pswd-2"
                      className={this.classes.textField}
                      onChange={this.handleChange("currentUser","pswd2")}
                      value={this.state.currentUser.pswd2}

                    />
                  </ItemGrid>

                </Grid>
                </div>
            }
            footer={((this.state.currentUser.pswd.length>0)&&(this.state.currentUser.pswd===this.state.currentUser.pswd2))?
              <Button  onClick={this.handleEdit.bind(this)}>
                Valider</Button>:
              <Button  disabled>  Valider</Button>
            }
          />
        </ItemGrid>
        <ItemGrid xs={12} sm={12} md={4}>
          <ProfileCard
            avatar={globale.backend_server.uri +"/"+this.state.currentUser.avatar}
            footer={
              <div>

              <input
                   accept="image/*"
                   id="raised-button-file"
                   type="file"
                   onChange={this.changeAvatar.bind(this)}
                   hidden
                 />
                 <label htmlFor="raised-button-file">
                   <Button variant="raised"  component="span">
                     modifier avatar
                   </Button>
                 </label>
                 <br />
                 {(this.state.loading === true)?
                   <CircularProgress className={this.classes.progress} style={{ color: red[500] }} thickness={7}/>
                 :null}
                 </div>
            }
          />
        </ItemGrid>
        </Grid>
        {this.state.currentUser.role === "super-admin" ?
        <Grid container>
        <ItemGrid xs={6} sm={6} md={6}>
        <ExpansionPanel>
           <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
             <Typography className={this.props.classes.heading}>Ajouter Un utilisateur</Typography>
           </ExpansionPanelSummary>
           <ExpansionPanelDetails>
             <Typography>
             <Grid container>
               <ItemGrid xs={12} sm={12} md={12}>
                 <TextField
                   label="nom utilisateur"

                   id="newUser-username"
                   className={this.classes.textField}
                   onChange={this.handleChange("newUser","username")}
                     value={this.state.newUser.username}


                 />
               </ItemGrid>
             <ItemGrid xs={12} sm={12} md={12}>
             <TextField
                       id="select-role"
                       select
                       label="role"
                       className={this.classes.textField}
                       value={this.state.newUser.role}
                       onChange={this.handleChange("newUser","role")}
                       SelectProps={{
                         MenuProps: {
                           className: this.classes.menu,
                         }
                       }}
                       margin="normal"
                     >
                       {roles.map(option => (
                         <MenuItem key={option.value} value={option.value}>
                           {option.label}
                         </MenuItem>
                       ))}
                     </TextField>

             </ItemGrid>
               </Grid>

             <Grid container>
               <ItemGrid xs={12} sm={12} md={6}>
               <TextField
                 label="mots de passe"
                 id="psw1"
                 className={this.classes.textField}
                 onChange={this.handleChange("newUser","pswd")}
                 value={this.state.newUser.pswd}
                 type="password"

               />
               </ItemGrid>
               <ItemGrid xs={12} sm={12} md={6}>
               <TextField
                 label="confirmer"
                 id="psw2"
                 className={this.classes.textField}
                 onChange={this.handleChange("newUser","password2")}
                 value={this.state.newUser.password2}
                 type="password"

               />
               </ItemGrid>
             </Grid>
             </Typography>
           </ExpansionPanelDetails>
           <ExpansionPanelActions>
{((this.state.newUser.pswd === this.state.newUser.password2) && (this.state.newUser.pswd.length > 0))?<Button size="small"  onClick={this.addUser.bind(this)}>Ajouter</Button>:
<Button size="small"  onClick={this.addUser.bind(this)} disabled>Ajouter</Button>
}        </ExpansionPanelActions>
          </ExpansionPanel>
            </ItemGrid>
            <ItemGrid xs={6} sm={6} md={6}>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <Typography className={this.props.classes.heading}>Modifier Un utilisateur</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
              <Grid container>
              <ItemGrid xs={12} sm={12} md={12}>
              <TextField
                        id="select-username"
                        select
                        label="utilisateur"
                        className={this.classes.textField}
                        value={this.state.updateUser.username}
                        onChange={this.handleChange("updateUser","username")}
                        SelectProps={{
                          MenuProps: {
                            className: this.classes.menu,
                          }
                        }}
                        margin="normal"
                      >
                        {this.state.users.map((option,index) => (
                          <MenuItem value={option.username}>
                            {option.username}
                          </MenuItem>
                        ))}
                </TextField>
              </ItemGrid>
              <ItemGrid xs={12} sm={12} md={12}>
              <TextField
                        id="select-role-update"
                        select
                        label="role"
                        className={this.classes.textField}
                        value={this.state.updateUser.role}
                        onChange={this.handleChange("updateUser","role")}
                        SelectProps={{
                          MenuProps: {
                            className: this.classes.menu,
                          }
                        }}
                        margin="normal"
                      >
                        {roles.map(option => (
                          <MenuItem key={option.value} value={option.value}>
                            {option.label}
                          </MenuItem>
                        ))}
                      </TextField>

              </ItemGrid>
              </Grid>
              </ExpansionPanelDetails>
              <ExpansionPanelActions>
                {(this.state.updateUser.username.length > 0) ?
                  <Button size="small" onClick={this.handleUpdate.bind(this)}>Modifier</Button>:
                  <Button size="small" disabled>Modifier</Button>}
              </ExpansionPanelActions>
            </ExpansionPanel>
            </ItemGrid>
            </Grid>:null}
            <Snackbar
              place="tr"
              color={this.state.color}
              icon={AddAlert}
              message={this.state.content}
              open={this.state.tr}
              closeNotification={() => this.setState({ tr: false })}
              close
            />
      </div>
  );}
}

UserProfile.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(profileStyles)(UserProfile);
