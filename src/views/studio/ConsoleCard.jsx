import React from 'react';
import PropTypes from 'prop-types';
import {
    withStyles,
    Card,
    CardContent,
    CardActions
  } from "material-ui";
import socketIOClient from 'socket.io-client'

const styles = {
  card: {
    minWidth: 275,
    borderRadius:"10px",
    height:"450px",
    backgroundColor:"black"
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  proposition:{height:"33%",textAlign:"center",border:"solid 0.5px gray",
  borderRadius:"10px",marginBottom:"5px",
  lineHeight:3.5,backgroundColor:"oldlace"}
};
let socket = null;
class ConsoleCard extends React.Component {
constructor(props){
    super(props);
    this.classes = props.classes;
    this.state = {
        contents:[{color: "red",
        content: "first message",
        event: "first"}]
    }
//    container = this;
  
}
componentWillMount(){
  socket = socketIOClient('http://localhost:4070');
  if(socket){
    socket.emit("console",{console:"ok"});
  }else console.log("error")

 socket.on('console', message => { 
   let contents = this.state.contents.slice(0);
   switch (message.id){
     case "join": contents.push({event:"join",content:message.content,color:"lime"});break;
     case "comment":  contents.push({event:"comment",content:message.content,color:"orange"});break;
     case "test":  contents.push({event:"test",content:message.content,color:"red"});break;
     default:contents.push({event:"non reconnu",content:"message non reconnu",color:"red"});
    }
    console.log(contents);
    this.setState({contents:contents});
 });
}

render(){
  console.log(this.state);
  return (
    <div>
      <Card className={this.classes.card}>
        <CardContent>
          {this.state.contents.map((value,key)=>(
            <div style={{width:"100%"}}><span style={{color:value.color}}>{value.event}@</span>
            {value.content}
            </div>
          ))}           
          </CardContent>
        <CardActions>
        </CardActions>
      </Card>
    </div>
  );
}}

ConsoleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(ConsoleCard);


