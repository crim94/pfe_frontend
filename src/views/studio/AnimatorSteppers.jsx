import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import MobileStepper from 'material-ui/MobileStepper';
import Button from 'material-ui/Button';
import KeyboardArrowLeft from 'material-ui-icons/KeyboardArrowLeft';
import KeyboardArrowRight from 'material-ui-icons/KeyboardArrowRight';

const styles = {
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
};

class AnimatorSteppers extends React.Component {
 constructor(props){
   super(props);
   this.state = {
    activeStep: props.activeStep,
    steps:props.steps,
    disabled:props.disabled
  };
 }
  
componentWillReceiveProps(props){
  this.setState( {
    activeStep: props.activeStep,
    steps:props.steps,
    disabled:props.disabled
  });
}
  handleNext = () => {
    this.props.nextClick();
    this.setState({
      activeStep: this.state.activeStep + 1,
    });
  };

  handleBack = () => {
    this.setState({
      activeStep: this.state.activeStep - 1,
    });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <MobileStepper
        variant="dots"
        steps={this.state.steps}
        position="static"
        activeStep={this.state.activeStep}
        className={classes.root}
        nextButton={
          <Button size="small" onClick={this.handleNext} disabled={this.state.activeStep === this.state.steps - 1 || this.state.disabled}>
            suivant
            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
          </Button>
        }
        backButton={
          <Button disabled size="small" onClick={this.handleBack}>
            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            retour
          </Button>
        }
      />
    );
  }
}

AnimatorSteppers.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AnimatorSteppers);