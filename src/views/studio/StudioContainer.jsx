import React from "react"
import { withStyles,Grid } from "material-ui";
import Iframe from 'react-iframe';
import {ItemGrid} from "components";
import InfoCard from "./InfoCard";
import ConsoleCard from "./ConsoleCard";
import {Viewer} from "components";
const styles = {
  flex: {
    flex: 1,
  }
};

class StudioContainer extends React.Component{
  render() {
    return(
       <Grid container>
      <Viewer />
       <ItemGrid xs={5} sm={6} md={5} >
       <div style={{marginTop:"2em",width:"100%",height:"500px"}}>
       <InfoCard whoAmi="editeur" />
       </div>
       </ItemGrid>
       <ItemGrid xs={6} sm={6} md={6} >
       <div style={{marginTop:"2em",width:"100%",height:"500px"}}>
       <ConsoleCard />
       </div>
       </ItemGrid>
       
       </Grid>
    );
  }
};
export default withStyles(styles)(StudioContainer);
