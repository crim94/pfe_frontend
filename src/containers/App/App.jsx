import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { withStyles } from "material-ui";

import { Header, Footer, Sidebar } from "components";

import appRoutes from "routes/app.jsx";

import appStyle from "variables/styles/appStyle.jsx";

import image from "assets/img/sidebar-2.jpg";
import logo from "assets/img/ooredoo.svg";
import logochif from "assets/img/logochif.png";

const switchRoutes = (
  <Switch>
   
    {appRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      else return <Route path={prop.path} component={prop.component} key={key}/>;
    })}

  </Switch>
);

class App extends React.Component {
  state = {
    mobileOpen: false,
    mainOpen:true
  };
  handleDrawerToggle = () => {
    this.setState({mainOpen:this.state.mobileOpen, mobileOpen: !this.state.mobileOpen });
  };
  getRoute() {
    return this.props.location.pathname !== "/maps";
  }
  componentDidMount() {
    document.body.style.backgroundColor = "snow";

    if(! localStorage.getItem("admin-token")){
      this.props.history.push('/',null);

    };
    if(navigator.platform.indexOf('Win') > -1){
//      const ps = new PerfectScrollbar(this.refs.mainPanel);
    }
  }

  componentDidUpdate() {
    this.refs.mainPanel.scrollTop = 0;
  }
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={appRoutes}
          logoText={"Chifco smartgame"}
          logo={logo}
          headlogo={logochif}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="red"
          {...rest}
        />
        <div className={classes.mainPanel} ref="mainPanel">
        <Header
          routes={appRoutes}
          handleDrawerToggle={this.handleDrawerToggle}
          {...rest}
        />
          {/* On the /maps route we want the map to be on full screen - this is not possible if the content and conatiner classes are present because they have some paddings which would make the map smaller */}
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          ) : (
            <div className={classes.map}>{switchRoutes}</div>
          )}
          {this.state.mainOpen ? <Footer/> : null}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(appStyle)(App);
