import React from "react";
import PropTypes from "prop-types";
import {withStyles } from "material-ui";
import ooredoo from "../../assets/img/ooredoo.png";
import Grid from 'material-ui/Grid';
import classNames from 'classnames';

import footerStyle from "variables/styles/footerlogin";

function LoginFooter({ ...props }) {
  const { classes } = props;
  return (
    <footer className={classes.footerlogin} >
      <div >
      <Grid container spacing={24}>

      <Grid item xs={6} className={classes.red}>
      @Copyright Chifco 2018
       </Grid>
       <Grid item xs={6} className={classNames(classes.center,classes.snow)}>
       <img src={ooredoo} className={classes.img } alt="ooredoo"/>
       </Grid>

       </Grid>
      </div>
    </footer>
  );
}

LoginFooter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(footerStyle)(LoginFooter);
