import React from "react";
import PropTypes from "prop-types";
import {  withStyles } from "material-ui";

import footerStyle from "variables/styles/footerStyle";

function Footer({ ...props }) {
  const { classes } = props;
  return (
    <footer className={classes.footer} >
      <div className={classes.container}>
        <div className="text-center">
            <span className="pistache">Chifco</span> 2018
         </div>

      </div>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(footerStyle)(Footer);
