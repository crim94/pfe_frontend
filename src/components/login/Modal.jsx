import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Dialog from 'material-ui/Dialog';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import CloseIcon from 'material-ui-icons/Close';
import Slide from 'material-ui/transitions/Slide';
import Paper from 'material-ui/Paper';
import axios from 'axios';
import globale from "../../config.js";
import QuestionInput from "views/Session/QuestionInput.jsx";
const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
  root:{
    width:"60% !important",
    top:"5% !important",
    left:"20% !important",
    height:"80% !important"

  }
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}
const questionsTemplate = [{
  content: "new question",
  categorie: "categorie",
  propositions:[
    {content:"first prop",estCorrecte:false},
    {content:"second prop",estCorrecte:false},
    {content:"last prop",estCorrecte:false}
  ]
},{
   content: "new question",
   categorie: "categorie",
   propositions:[
     {content:"first prop",estCorrecte:false},
     {content:"second prop",estCorrecte:false},
     {content:"last prop",estCorrecte:false}
   ]
 },{
    content: "new question",
    categorie: "categorie",
    propositions:[
      {content:"first prop",estCorrecte:false},
      {content:"second prop",estCorrecte:false},
      {content:"last prop",estCorrecte:false}
    ]
  }];
class FullScreenDialog extends React.Component {
  constructor(props){
    super(props);
    var questions = questionsTemplate.slice(0);
      if (props.questions.length !== 0) questions = props.questions
    this.state = {
      open: this.props.visible,
      index:this.props.index,
      etats:[],
      categories:[],
      valides:0,
      questions:questions
    }
  };

  handleSupprimer(key){
    console.log("handlesupp");
    let questions =this.state.questions.slice(0);
    let etats = this.state.etats.slice(0);
    var valide = etats[key];
    etats.splice(key,1);
    questions.splice(key,1);
    let valides = this.state.valides;
    if(valide === 1)valides--;
    this.setState({questions,etats,valides});
  };
  updateState = (key,question) => {
    let questions = this.state.questions.slice(0);
    questions[key]= Object.assign({},question);
    this.setState({questions},function(){
      console.log("updateState");
    });
  };
  countValide(bool,key){
    console.log("count");
      let etats = this.state.etats.slice(0);
      (bool===true)?etats[key]=1:etats[key]=0;
      let valides = etats.reduce(function(memo, val) {
          return memo + val;
          });
          this.setState({etats,valides},function(){
            console.log(this.state.valides);
          });
  }
 


  handleClickOpen = () => {
    this.setState({ open: true });
  };
  componentDidMount(){
      var self=this;
      var mytoken = localStorage.getItem("admin-token");
      let api=globale.backend_server.uri+   globale.API.CATEGORIE_API+"/getALL";
      axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} })
         .then(function (response) {
           self.setState({
             categories:response.data
           });
         });
  }

  handleClose = () => {
    this.setState({ open: false },function(){
      var estValide=false;
      if (this.state.valides === this.state.questions.length)estValide=true;
      this.props.closer(this.state.index,this.state.questions,estValide);
    });
  };
  componentWillReceiveProps(props){
    console.log("in receive");
    var questionsTemplate=[{
      content: "new question",
      categorie: "categorie",
      propositions:[
        {content:"first prop",estCorrecte:false},
        {content:"second prop",estCorrecte:false},
        {content:"last prop",estCorrecte:false}
      ]
    },{
       content: "new question",
       categorie: "categorie",
       propositions:[
         {content:"first prop",estCorrecte:false},
         {content:"second prop",estCorrecte:false},
         {content:"last prop",estCorrecte:false}
       ]
     },{
        content: "new question",
        categorie: "categorie",
        propositions:[
          {content:"first prop",estCorrecte:false},
          {content:"second prop",estCorrecte:false},
          {content:"last prop",estCorrecte:false}
        ]
      }];
    console.log(props.index);
    console.log(questionsTemplate);
    var questions = questionsTemplate.slice(0);
   
    if (props.questions.length > 0){
      questions = props.questions;
    } 
    this.setState({open:props.visible,index:props.index,questions:questions});
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Dialog
          fullScreen
          open={this.state.open}
          onClose={this.handleClose}
          transition={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
              <Typography variant="title" color="inherit" className={classes.flex}>
                Editeur de quiz
              </Typography>
              {this.state.valides < 3 ?
              <Button color="inherit" onClick={this.handleClose} disabled>
                Enregistrer
              </Button> : <Button color="inherit" onClick={this.handleClose}>
                Enregistrer
              </Button> }
            </Toolbar>
          </AppBar>
          <Paper>

          {
            this.state.questions.map((question,key) => (
            <QuestionInput question={question} numero={key} supprimer={this.handleSupprimer.bind(this)}
            update={this.updateState.bind(this)} categories={this.state.categories}
            validation={this.countValide.bind(this)}
            />
             ))
           }
           </Paper>
        </Dialog>
      </div>
    );
  }
}

FullScreenDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  visible:PropTypes.bool.isRequired,
  closer:PropTypes.func.isRequired,
  index:PropTypes.number.isRequired,
  questions:PropTypes.array.isRequired
};
export default withStyles(styles)(FullScreenDialog);
