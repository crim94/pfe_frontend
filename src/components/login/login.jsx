import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
import classNames from 'classnames';
import admin from '../../assets/img/admin.png';
import TextField from 'material-ui/TextField';
import Button from "material-ui/Button";
import Send from 'material-ui-icons/Send';
import axios from 'axios';
import LoginFooter from "../Footer/loginFooter";
import {Snackbar} from "components";
import { AddAlert } from "material-ui-icons";
import loginStyle from "../../variables/styles/loginStyle.jsx";
import globale from "../../config.js";

class Login extends React.Component{
  constructor(props) {
    super(props);
    document.body.style.backgroundColor = "rgba(237, 29, 39, 0.75)";
    this.state = {
      username:' username',
      password:' password',
      tr: false,
      content: "msg",
      color:"primary"
    };
  }
  componentDidMount() {
    if(localStorage.getItem("admin-token")){
      this.props.history.push('/dashboard',null);

    };}
  showNotification(place) {
    var x = [];

    x[place] = true;
    this.setState(x);
    setTimeout(
      function() {
        x[place] = false;
        this.setState(x);
      }.bind(this),
      6000
    );
  }
  handleChange = nom => event => {
  this.setState({
    [nom]: event.target.value,
  });


};
handleSubmit(){
  var self=this;
  let api=globale.backend_server.uri+   globale.API.LOGIN_API+"/login";
  axios.post(api, this.state)
     .then(function (response) {
       var user=JSON.stringify(response.data.user);
       console.log(user);
       //selon le type d utilisateur on va ajouter un token : role-token
       localStorage.setItem("admin-token",response.data.token);
       self.setState({
         color:"info",
         content: "vous etes inscrit"
       })
       self.showNotification("tr");
       localStorage.setItem('user', user);
       self.props.history.push('/dashboard',null);

     })
     .catch(function (error) {
       self.setState({
         color:"danger",
         content: "Verifiez svp"
       })
       self.showNotification("tr");
     });
}
  render()
  {return (
    <div>
    <div className={this.props.classes.conteneur}>
      <Paper className={this.props.classes.root} elevation={15}>
        <Typography variant="headline" component="h3">
        <div className={this.props.classes.row}>

        <Avatar
              alt="Admin Logo"
              src={admin}
              className={classNames(this.props.classes.avatar, this.props.classes.bigAvatar)}
            />
        </div>
        </Typography>
        <hr />
        <div className={this.props.classes.main}>
        <form className={this.props.classes.container} noValidate autoComplete="off">
        <TextField
          id="username"
          label="nom utilisateur"
          className={this.props.classes.textField}
          value={this.state.nom}
          onChange={this.handleChange('username')}
          margin="normal"
        />
        <TextField
          id="password"
          label="password"
          className={this.props.classes.textField}
          value={this.state.nom}
          onChange={this.handleChange('password')}
          type="password"
          margin="normal"
        />
        <div className={this.props.classes.row}>
        <Button variant="raised" className={this.props.classes.button}
        onClick={this.handleSubmit.bind(this)}>
                s'authentifier<Send className={classNames(this.props.classes.rightIcon, this.props.classes.iconSmall)} />

        </Button>
        </div>
        </form>
        </div>
        <Snackbar
          place="tr"
          color={this.state.color}
          icon={AddAlert}
          message={this.state.content}
          open={this.state.tr}
          closeNotification={() => this.setState({ tr: false })}
          close
        />

      </Paper>
    </div>
    <LoginFooter />
    </div>
  );}
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(loginStyle)(Login);
