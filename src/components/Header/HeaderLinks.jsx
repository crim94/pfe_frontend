import React from "react";


import {
  withStyles,
  IconButton,





  Hidden
} from "material-ui";
import {ExitToApp,NavigateBefore } from "material-ui-icons";


import headerLinksStyle from "variables/styles/headerLinksStyle";

class HeaderLinks extends React.Component {
  constructor(props) {
    super(props);
   
    this.state = {
    open: false
  };
}
  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleLogout= () => {
    localStorage.clear();
    window.location.replace("/");
  }
  render() {
    const { classes } = this.props;
   
    return (
      <div>
         <IconButton
          color="inherit"
          aria-label="retour"
          onClick={() => {if(this.props.router)this.props.router.goBack()}}
          className={classes.buttonLink}
        >
          <NavigateBefore className={classes.links} />
          <Hidden mdUp>
            <p className={classes.linkText}>retour</p>
          </Hidden>
        </IconButton> 
        <IconButton
          color="inherit"
          aria-label="Deconnexion"
          onClick={this.handleLogout.bind(this)}
          className={classes.buttonLink}
        >
          <ExitToApp className={classes.links} />
          <Hidden mdUp>
            <p className={classes.linkText}>Deconnexion</p>
          </Hidden>
        </IconButton>
     
        </div>
    );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
