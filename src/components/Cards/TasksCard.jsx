import React from "react";
import PropTypes from "prop-types";
import {
  withStyles,
  Card,
  CardContent,
  CardHeader,
  Typography,
  Tabs,
  Tab
} from "material-ui";
import { Assignment,Help } from "material-ui-icons";

import { Tasks } from "components";

import { BarChart,XAxis,YAxis,Bar,Legend,Tooltip,CartesianGrid} from 'recharts';
import tasksCardStyle from "variables/styles/tasksCardStyle";
import axios from 'axios';
import globale from "../../config.js";
const data_win_los = [
  {name: '23/12/2016',nb_winner:4,nb_loser:3 },
  {name: '23/12/2016',nb_winner:8,nb_loser:1 },
  {name: '23/12/2016',nb_winner:3,nb_loser:3 },
  {name: '23/12/2016',nb_winner:9,nb_loser:0},
  {name: '23/12/2016',nb_winner:4,nb_loser:3 },
]
class TasksCard extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value: 0,
      players:[]
    };
  }
  componentWillMount(){
    var self=this;
    var mytoken = localStorage.getItem("admin-token");
    let api=globale.backend_server.uri+":"+globale.backend_server.port+globale.API.STATISTIC+"/topFive";
    axios.get(api,{ headers: { Authorization: "Bearer "+ mytoken} }).then(function(response){
      if(response.status===200){
        //afficher notification & vider questions
      self.setState({players:response.data});
      }
    }).catch(function(error){
      console.log(error);
    });
  }
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.card}>
        <CardHeader
          classes={{
            root: classes.cardHeader,
            title: classes.cardTitle,
            content: classes.cardHeaderContent
          }}
          title="Autre:"
          action={
            <Tabs
              classes={{
                flexContainer: classes.tabsContainer
              }}
              value={this.state.value}
              onChange={this.handleChange}
              indicatorClassName={classes.displayNone}
              textColor="inherit"
            >
              <Tab
                classes={{
                  wrapper: classes.tabWrapper,
                  rootLabelIcon: classes.labelIcon,
                  label: classes.label,
                  rootInheritSelected: classes.rootInheritSelected
                }}
                icon={<Assignment className={classes.tabIcon} />}
                label={"Top 5"}
              />
              <Tab
                classes={{
                  wrapper: classes.tabWrapper,
                  rootLabelIcon: classes.labelIcon,
                  label: classes.label,
                  rootInheritSelected: classes.rootInheritSelected
                }}
                icon={<Help className={classes.tabIcon} />}
                label={"feedback"}
              />
            </Tabs>
          }
        />
        <CardContent>
          {this.state.value === 0 && (
            <Typography component="div">
              <Tasks
                tasksIndexes={[0, 1, 2, 3,4]}
                tasks={this.state.players}
              />
            </Typography>
          )}
           {this.state.value === 1 && (
            <Typography component="div">
              <BarChart width={450} height={300} data={data_win_los}>
               <CartesianGrid strokeDasharray="3 3"/>

        <XAxis dataKey="name"/>
        <YAxis />
        <Tooltip/>
       <Legend />
        <Bar type="monotone" stackId="a" dataKey="nb_winner" barSize={30} fill="lightgreen"
          />
        <Bar type="monotone" stackId="a" dataKey="nb_loser" barSize={30} fill="orange"
          />
         </BarChart>
            </Typography>
          )}
          
        </CardContent>
      </Card>
    );
  }
}

TasksCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(tasksCardStyle)(TasksCard);
