import React from "react";
import {
  withStyles,
  Table,
  TableBody,
  TableCell,
  TableRow
} from "material-ui";

import PropTypes from "prop-types";

import tasksStyle from "variables/styles/tasksStyle.jsx";
import globale from "../../config.js";

class Tasks extends React.Component {
  state = {
    checked: this.props.checkedIndexes
  };
  handleToggle = value => () => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  };
  render() {
    const { classes, tasks } = this.props;
    return (
      <Table className={classes.table}>
        <TableBody>
          {tasks.map((value,index) => (
            <TableRow key={index} className={classes.tableRow}>
              <TableCell className={classes.tableCell}>
                {index+1}
              </TableCell>
              <TableCell className={classes.tableCell}>
                {value.username}
              </TableCell>
              <TableCell className={classes.tableCell}>
                {value.profit_tot}
              </TableCell>
              <TableCell className={classes.tableActions}>
                <img src={globale.backend_server.uri+   value.avatar_uri} style={{width:40,height:40,borderRadius:30}} alt="avatar"/>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }
}

Tasks.propTypes = {
  classes: PropTypes.object.isRequired,
  tasksIndexes: PropTypes.arrayOf(PropTypes.number),
  tasks: PropTypes.arrayOf(PropTypes.node)
};

export default withStyles(tasksStyle)(Tasks);
