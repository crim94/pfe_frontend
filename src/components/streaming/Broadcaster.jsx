import React, { Component } from 'react';
import KurrentoClient from "../../assets/kurento_asset_js/kurento-client/js/kurento-client.js";
import kurentoUtils from "../../assets/kurento_asset_js/kurento-utils/js/kurento-utils.js";

import PropTypes from 'prop-types';
import globale from "../../config.js";
import SocketIOClient from 'socket.io-client';
import {
  withStyles,
  Card,
  CardContent,
  CardHeader,CardMedia,Button,
  CardActions,
  Typography} from "material-ui";
  var webRtcPeer;
  var ws ;
  function onError(error) {
    if(error)
    {
      console.error(error);
      stop();
    }
  }
  function presenterResponse(message) {
    if (message.response != 'accepted') {
      var errorMsg = message.message ? message.message : 'Unknow error';
      console.warn('Call not accepted for the following reason: ' + errorMsg);
    } else {
      webRtcPeer.processAnswer(message.sdpAnswer);
    }
  }
  
  function viewerResponse(message) {
    if (message.response != 'accepted') {
      var errorMsg = message.message ? message.message : 'Unknow error';
      console.warn('Call not accepted for the following reason: ' + errorMsg);
    } else {
      webRtcPeer.processAnswer(message.sdpAnswer);
    }
  }
  
  function presenter(videoInput) {
    if (!webRtcPeer) {
  
  
      var options = {
        localVideo: videoInput,
        onicecandidate : onIceCandidate
        }
  
      webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, function(error) {
        if(error) return onError(error);
  
        this.generateOffer(onOfferPresenter);
      });
    }
  }
  
  function onOfferPresenter(error, offerSdp) {
      if (error) return onError(error);
  
    var message = {
      id : 'presenter',
      sdpOffer : offerSdp
    };
    sendMessage(message);
  }
  
  function viewer(videoRemote) {
    if (!webRtcPeer) {
  
      var options = {
        remoteVideo: videoRemote,
        onicecandidate : onIceCandidate
      }
  
      webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options, function(error) {
        if(error) return onError(error);
  
        this.generateOffer(onOfferViewer);
      });
    }
  }
  
  function onOfferViewer(error, offerSdp) {
    if (error) return onError(error)
  
    var message = {
      id : 'viewer',
      sdpOffer : offerSdp
    }
    sendMessage(message);
  }
  
  function onIceCandidate(candidate) {
       console.log('Local candidate' + JSON.stringify(candidate));
  
       var message = {
          id : 'onIceCandidate',
          candidate : candidate
       }
       sendMessage(message);
  }
  function sendMessage(message) {
    var jsonMessage = JSON.stringify(message);
    console.log('Senging message: ' + jsonMessage);
    ws.emit("message",jsonMessage);
  
  }
  
  //#endregion
  const styles = {
    card: {
      maxWidth: 600,
    },
    media: {
      // ⚠️ object-fit is not supported by IE 11.
      objectFit: 'cover',
    },
  };
  class Broadcaster extends Component {
      constructor(props){
          super(props);
          this.classes = props.classes;
  
  
      }
    shouldComponentUpdate() {
      return false;
    }
    componentDidMount() {
      //initialize plugin
      this.localStream = this.refs.localVideo;
  
  
    }
    componentWillReceiveProps(nextProps){
  
    }
  
  
  
  
  handleClick(){
     ws = SocketIOClient(globale.streaming_server.uri);
    ws.on('connect', () => {
      console.log("connected");
      presenter(this.refs.localVideo);
    });
    ws.on('connect_error', () => {
      console.log("err");
    });
    ws.on('message',function(message) {
    
    
      var parsedMessage = JSON.parse(message);
      console.info('Received message: ' + parsedMessage.id);
      switch (parsedMessage.id) {
      case 'presenterResponse':
        presenterResponse(parsedMessage);
        break;
      case 'viewerResponse':
        viewerResponse(parsedMessage);
        break;
      case 'stopCommunication':
        dispose();
        break;
      case 'iceCandidate':
        webRtcPeer.addIceCandidate(parsedMessage.candidate)
        break;
      default:
        console.error('Unrecognized message', parsedMessage);
      }
    });
   
  
  }
  handleStop(){
    if (webRtcPeer) {
      webRtcPeer.dispose();
      webRtcPeer = null;
    }
  
    if(pipeline){
      pipeline.release();
      pipeline = null;
    }
  }
  
    render() {
        const classes = this.classes;
      return (
          <Card className={classes.card}>
        
  
            <CardContent>
            <video ref="localVideo" autoPlay width="480px" height="360px" ></video>
  
            </CardContent>
         
          <CardActions>
  
  
            <Button size="small" color="primary" onClick={this.handleClick.bind(this)}>
            BroadCaster
            </Button>
  
          </CardActions>
        </Card>
      );
    }
  }
Broadcaster.propTypes = {
    classes: PropTypes.object.isRequired
  };

  export default withStyles(styles)(Broadcaster);
